{ config, pkgs, ... }:

{
  imports = [
    ../modules/common.nix
    ../modules/kitty.nix
    ../modules/yubikey.nix
    # ../modules/mullvad_vpn.nix
    ../modules/proton_vpn.nix
    ../modules/ryzen_gaming.nix
  ];

  hardware.bluetooth.enable = true; # enables support for Bluetooth
  hardware.bluetooth.powerOnBoot = true; # powers up the default Bluetooth controller on boot
  services.blueman.enable = true;

  fileSystems."/mnt/data" = {
    device = "192.168.1.153:/data";
    fsType = "nfs";
  };

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.grub.splashImage = ../../assets/grub_splash_image.jpg;

  boot.kernelPackages = pkgs.linuxPackages_latest;

  boot.binfmt.emulatedSystems = [
    "aarch64-linux"
    "armv7l-linux"
    "riscv64-linux"
  ];

  services.flatpak.enable = true;
  services.snap.enable = true;


  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Enable the Plasma Desktop Environment.
  services.displayManager.sddm = {
    enable = true;
    wayland.enable = true;
  };
  services.desktopManager.plasma6.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable automatic login for the user.
  services.displayManager.autoLogin.enable = true;
  services.displayManager.autoLogin.user = "luis";

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # Fonts
  fonts.packages = with pkgs; [
    nerd-fonts.jetbrains-mono
    nerd-fonts.hack
    noto-fonts-emoji
    siji
  ];

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    home-manager
    wl-clipboard
    vim
    curl
    docker-compose
    git
    wget
    alsa-utils
    radeontop
  ];

  home-manager.backupFileExtension = "backup";

  # Virtualisation
  virtualisation.docker = {
    enable = true;
    extraOptions = '' --default-address-pool base=172.240.0.0/16,size=24 '';
  };

  # Containers
  virtualisation.oci-containers = {
    backend = "podman"; # "docker" or "podman"
    containers.acestream = {
      image = "blaiseio/acelink";
      ports = [ "6878:6878" ];
    };
  };

  # List services that you want to enable:
  programs.dconf.enable = true;
  services.trezord.enable = true;
  hardware.ledger.enable = true;

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05"; # Did you read the comment?

}
