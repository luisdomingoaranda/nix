{ config
, pkgs
, lib
, ...
} @ inputs:

let
  myPkgs = inputs.mynixpkgs.packages.${inputs.system};
in
{
  imports = [
    ../modules/common.nix
    ../modules/i3.nix
    ../modules/pipewire.nix
    ../modules/kitty.nix
    ../modules/virtmanager.nix
    ../modules/yubikey.nix
  ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only
  boot.loader.grub.splashImage = ../../assets/grub_splash_image.jpg;

  boot.binfmt.emulatedSystems = [
    "aarch64-linux"
    "armv7l-linux"
    "riscv64-linux"
  ];

  # Fonts
  fonts.packages = with pkgs; [
    nerdfonts
    noto-fonts-emoji
    siji
  ];

  environment.systemPackages = with pkgs; [
    home-manager
    git
    wget
    binutils
    udisks # udisks2
    udiskie # removable disk automounter for udisks
    ntfs3g # ntfs driver
    jmtpfs # A FUSE filesystem for MTP devices like Android phones
    openfortivpn # Vpn client
    htop
    stow
    dconf # gnome configuration manager
    #virtualboxWithExtpack  # virtualbox with extensions
    docker-compose
    ledger-live-desktop
    flatpak
    nemo
    nixos-generators
  ];

  # Virtualisation
  virtualisation.docker = {
    enable = true;
    extraOptions = '' --default-address-pool base=172.240.0.0/16,size=24 '';
  };

  # List services that you want to enable:
  programs.dconf.enable = true;
  services.trezord.enable = true;
  hardware.ledger.enable = true;
  services.udisks2.enable = true;
  systemd.user.services."udiskie" = {
    enable = true;
    description = "udiskie to automount removable media";
    wantedBy = [ "default.target" ];
    serviceConfig = {
      Type = "simple";
      Restart = "always";
      ExecStart = "${pkgs.udiskie}/bin/udiskie";
    };
  };

  services.udev.packages = [ pkgs.kingstvis ];

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.03"; # Did you read the comment?

}

