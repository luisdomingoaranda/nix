{ config
, pkgs
, lib
, username
, nerdFont
, home-manager
, myPkgs
, ...
} @ inputs:

{
  imports = [
    ../modules/common.nix
    ../modules/i3.nix
    ../modules/pipewire.nix
    ../modules/kitty.nix
    ../modules/yubikey.nix
  ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only
  boot.loader.grub.splashImage = ../../assets/grub_splash_image.jpg;

  boot.binfmt.emulatedSystems = [
    "aarch64-linux"
    "armv7l-linux"
    "riscv64-linux"
  ];

  # Fonts
  fonts.packages = with pkgs; [
    (pkgs.nerdfonts.override { fonts = [ nerdFont ]; })
    noto-fonts-emoji
    siji
  ];

  # Packages
  environment.systemPackages = with pkgs; [
    # system should have a minimal number of packages
    # just enough to do basic editing and setup the rest of the system
    home-manager
    git
    wget
    binutils
    udisks # udisks2
    udiskie # removable disk automounter for udisks
    ntfs3g # ntfs driver
    jmtpfs # A FUSE filesystem for MTP devices like Android phones
    stow
    font-manager
    dconf # gnome configuration manager
    docker-compose
    flatpak
    nemo
    nixos-generators
  ];

  # Virtualisation
  virtualisation.docker = {
    enable = true;
    extraOptions = '' --default-address-pool base=172.240.0.0/16,size=24 '';
  };
  # Enable vmware tools
  virtualisation.vmware.guest.enable = true;
  virtualisation.vmware.guest.headless = true;

  programs.dconf.enable = true;
  services.trezord.enable = true;
  services.udisks2.enable = true;
  systemd.user.services."udiskie" = {
    enable = true;
    description = "udiskie to automount removable media";
    wantedBy = [ "default.target" ];
    serviceConfig = {
      Type = "simple";
      Restart = "always";
      ExecStart = "${pkgs.udiskie}/bin/udiskie";
    };
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.03"; # Did you read the comment?

}
