{ config
, pkgs
, username
, home-manager
, ...
} @ inputs:

let
  rpi4SecretsDir = ../../secrets/rpi4;
in
{
  imports = [
    ../modules/common.nix
    ../modules/ssh/ssh.nix
    ../modules/yubikey.nix
    ../modules/nextcloud.nix
    ../modules/jellyfin.nix
    home-manager.nixosModules.home-manager
  ];

  home-manager.users.${username}.home.stateVersion = "22.05";

  # Use the extlinux boot loader. (NixOS wants to enable GRUB by default)
  boot.loader.grub.enable = false;
  boot.loader.generic-extlinux-compatible.enable = true;
  boot.kernelPackages = pkgs.linuxPackages_latest // pkgs.linuxPackages_rpi4;

  # Enables wireless support via wpa_supplicant.
  # Read /etc/wpa_supplicant.conf
  networking.wireless.enable = true; # Enables wireless support via wpa_supplicant.

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.eth0.useDHCP = true;
  networking.interfaces.wlan0.useDHCP = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    wget
    vim
    git
    stow
    binutils
    htop
    file
    git-crypt # Transparent file encryption in git
    ungoogled-chromium
    wol # wake on lan
  ];

  # List services that you want to enable:
  services = {
    nginx = {
      enable = true;
      # virtualHosts."blog.example.com" = {
      #   enableACME = true;
      #   forceSSL = true;
      #   root = "/var/www/blog";
      #   locations."~ \.php$".extraConfig = ''
      #     fastcgi_pass  unix:${config.services.phpfpm.pools.mypool.socket};
      #   fastcgi_index index.php;
      #   '';
      # };
    };
  };

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [
    22
    80
    443
  ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.03"; # Did you read the comment?

}

