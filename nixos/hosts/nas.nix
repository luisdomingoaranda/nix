{ config, pkgs, ... }:

{
  imports = [
    ../modules/common.nix
    ../modules/pipewire.nix
    ../modules/kitty.nix
    ../modules/yubikey.nix
    ../modules/media_server.nix
    # ../modules/mullvad_vpn.nix
  ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.grub.splashImage = ../../assets/grub_splash_image.jpg;

  boot.binfmt.emulatedSystems = [
    "aarch64-linux"
    "armv7l-linux"
    "riscv64-linux"
  ];

  # Enable ZFS
  boot.supportedFilesystems = [ "zfs" ];
  boot.zfs.extraPools = [
    "nas_raidz2"
  ];

  services.zfs.autoScrub = {
    enable = true;
    interval = "monthly";
  };

  services.flatpak.enable = true;
  services.snap.enable = true;


  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;
  networking.hostId = "0000354b";

  # Enable the Plasma Desktop Environment.
  services.displayManager.sddm = {
    enable = true;
    wayland.enable = true;
  };
  services.desktopManager.plasma6.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable automatic login for the user.
  services.displayManager.autoLogin.enable = true;
  services.displayManager.autoLogin.user = "luis";

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # Allow insecure packages (TODO: remove)
  nixpkgs.config.permittedInsecurePackages = [
    "aspnetcore-runtime-6.0.36"
    "aspnetcore-runtime-wrapped-6.0.36"
    "dotnet-sdk-6.0.428"
    "dotnet-sdk-wrapped-6.0.428"
  ];

  # Fonts
  fonts.packages = with pkgs; [
    nerd-fonts.jetbrains-mono
    nerd-fonts.hack
    noto-fonts-emoji
    siji
  ];

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    inetutils # Collection of common network programs
    pciutils # Collection of programs for inspecting and manipulating configuration of PCI devices
    kdePackages.plasma-pa
    home-manager
    wl-clipboard
    vim
    curl
    docker-compose
    git
    wget
    ntfs3g # ntfs driver
    udisks # udisks2
    udiskie # removable disk automounter for udisks
    docker-compose
    libva-utils # run: vainfo (check integrated graphics driver)
  ];

  home-manager.backupFileExtension = "backup";

  # Virtualisation
  virtualisation.docker = {
    enable = true;
    extraOptions = '' --default-address-pool base=172.240.0.0/16,size=24 '';
  };

  # virtualisation.oci-containers = {
  #   backend = "podman"; # "docker" or "podman"
  #   containers.whoogle-search = {
  #     image = "benbusby/whoogle-search";
  #     autoStart = true;
  #     ports = [ "8080:5000" ]; #server locahost : docker localhost
  #   };
  # };

  # List services that you want to enable:
  programs.dconf.enable = true;
  services.trezord.enable = true;
  hardware.ledger.enable = true;


  # Enable intel graphics transcoding
  boot.kernelParams = [
    "i915.enable_guc=3" # Enable both GuC (Graphics microcontroller) and HuC (High-level microcontroller)
  ];

  hardware.graphics = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver
      intel-compute-runtime
      libvdpau-va-gl
      vpl-gpu-rt # Library to dispatch to a backing implementation
    ];
  };

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05"; # Did you read the comment?

}
