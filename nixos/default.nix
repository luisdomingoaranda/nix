{ self
, nixpkgs-unstable
, nixpkgs-stable
, home-manager-stable
, home-manager-unstable
, nixos-hardware
, nix-snapd
, ...
} @ inputs:

let
  # Nerd font options: https://github.com/ryanoasis/nerd-fonts
  nerdFont = "Inconsolata";
  # Base16 colorscheme options: https://github.com/tinted-theming/base16-schemes
  # How to style: https://github.com/chriskempson/base16/blob/main/styling.md
  base16scheme = "tokyo-night-terminal-dark";
in
{

  desktop =
    let
      username = "luis";
      system = "x86_64-linux";
      specialArgs = inputs
        // { home-manager = home-manager-unstable; }
        // { inherit username system nerdFont base16scheme; };
    in
    nixpkgs-unstable.lib.nixosSystem {
      inherit system specialArgs;
      modules = [
        ./hosts/desktop.nix
        ./hardware/desktop.nix
        home-manager-unstable.nixosModules.home-manager {
          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;
          home-manager.extraSpecialArgs = specialArgs;
          home-manager.users.${username} = {
            imports = [
              ../home-manager/hosts/desktop.nix
            ];
          };
        }
      ];
    };

  ryzen_mini =
    let
      username = "luis";
      system = "x86_64-linux";
      specialArgs = inputs
        // { home-manager = home-manager-unstable; }
        // { inherit username system nerdFont base16scheme; };
    in
    nixpkgs-unstable.lib.nixosSystem {
      inherit system specialArgs;
      modules = [
        ./hosts/ryzen_mini.nix
        ./hardware/ryzen_mini.nix
        nix-snapd.nixosModules.default
        home-manager-unstable.nixosModules.home-manager {
          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;
          home-manager.extraSpecialArgs = specialArgs;
          home-manager.users.${username} = {
            imports = [
              ../home-manager/hosts/ryzen_mini.nix
            ];
          };
        }
      ];
    };

  nas =
    let
      username = "luis";
      system = "x86_64-linux";
      specialArgs = inputs
        // { home-manager = home-manager-unstable; }
        // { inherit username system nerdFont base16scheme; };
    in
    nixpkgs-unstable.lib.nixosSystem {
      inherit system specialArgs;
      modules = [
        ./hosts/nas.nix
        ./hardware/nas.nix
        nix-snapd.nixosModules.default
        home-manager-unstable.nixosModules.home-manager {
          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;
          home-manager.extraSpecialArgs = specialArgs;
          home-manager.users.${username} = {
            imports = [
              ../home-manager/hosts/nas.nix
            ];
          };
        }
      ];
    };

  vm =
    let
      username = "luis";
      system = "x86_64-linux";
      specialArgs = inputs
        // { home-manager = home-manager-unstable; }
        // { inherit username system nerdFont base16scheme; };
    in
    nixpkgs-unstable.lib.nixosSystem {
      inherit system specialArgs;
      modules = [
        ./hosts/vm.nix
        ./hardware/vm.nix
        home-manager-unstable.nixosModules.home-manager {
          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;
          home-manager.extraSpecialArgs = specialArgs;
          home-manager.users.${username} = {
            imports = [
              ../home-manager/hosts/vm.nix
            ];
          };
        }
      ];
    };

  rpi4 =
    let
      username = "luis";
      system = "aarch64-linux";
      specialArgs = inputs
        // { home-manager = home-manager-unstable; }
        // { inherit username system nerdFont base16scheme; };
    in
    nixpkgs-unstable.lib.nixosSystem {
      inherit system specialArgs;
      modules = [
        ./hosts/rpi4.nix
        ./hardware/rpi4.nix
        nixos-hardware.nixosModules.raspberry-pi-4
      ];
    };

}
