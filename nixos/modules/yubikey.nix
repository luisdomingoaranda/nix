{ config
, pkgs
, username
, home-manager
, ...
} @ inputs:

{
  imports = [
    home-manager.nixosModules.home-manager
  ];

  # When trying to access a smart-card (i.e. yubikey) with GPG
  # you may need to stop the pcscd service
  # because GPG uses its own daemon for smartcards (scdaemon)
  # and they conflict.
  # When you need to use the pcscd daemon again (e.g. for ykman)
  # start again the service `systemctl start pcscd`
  # After stop/start plug the yubikey again.
  services.pcscd.enable = true; # PC/SC Smart Card Daemon
  services.udev.packages = [ pkgs.yubikey-personalization ];
  hardware.gpgSmartcards.enable = true;

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
    pinentryPackage = pkgs.pinentry-gtk2;
  };
}
