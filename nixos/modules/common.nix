{ config
, pkgs
, username
, home-manager
, ...
} @ inputs:

{
  imports = [
    ./zsh.nix
  ];

  # Flakes support
  nix = {
    package = pkgs.nixVersions.git;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
    settings = {
      auto-optimise-store = true;
    };
  };

  nixpkgs.config.allowUnfree = true;

  services.resolved.enable = false; # don't use resolved DNS resolver daemon
  networking = {
    hostName = "nixos"; # Define your host name
    networkmanager.dns = "none";
    nameservers = [
      # cloudfare DNS servers
      "1.1.1.1"
      "1.0.0.1"
      # Quad9 DNS servers
      "9.9.9.9"
      "149.112.112.112"
    ];
  };

  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings = {
      LC_ADDRESS = "es_ES.UTF-8";
      LC_IDENTIFICATION = "es_ES.UTF-8";
      LC_MEASUREMENT = "es_ES.UTF-8";
      LC_MONETARY = "es_ES.UTF-8";
      LC_NAME = "es_ES.UTF-8";
      LC_NUMERIC = "es_ES.UTF-8";
      LC_PAPER = "es_ES.UTF-8";
      LC_TELEPHONE = "es_ES.UTF-8";
      LC_TIME = "es_ES.UTF-8";
    };
  };

  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  # Set your time zone.
  time.timeZone = "Europe/Madrid";

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.${username} = {
    isNormalUser = true;
    extraGroups = [
      "wheel"
      "networkmanager"
      "libvirtd"
      "kvm"
      "uucp"
      "nextcloud"
      "audio"
      "video"
      "render"
      "input"
    ]; # whell -> enable sudo for user
    initialPassword = "nixos";
  };

}
