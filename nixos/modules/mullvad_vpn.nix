{ config
, pkgs
, username
, home-manager
, ...
} @ inputs:

{

  services.mullvad-vpn = {
    enable = true;
    package = pkgs.mullvad-vpn;
  };

}
