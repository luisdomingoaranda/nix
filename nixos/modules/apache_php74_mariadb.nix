{ config
, pkgs
, username
, nixpkgs-stable
, ...
} @ inputs:

let
  pkgs-stable = import nixpkgs-stable { system = inputs.system; };
  myPHP = pkgs-stable.php74.buildEnv {
    extensions = { enabled, all }:
      with all;
      [
        gd
        curl
        ldap
        soap
        exif
        mysqli
        pdo_mysql
        sqlsrv
        pdo_sqlsrv
        zip
      ] ++ enabled;
    extraConfig = ''
      upload_max_filesize = 10M
      memory_limit = 1024M
    '';
  };
in
{

  networking.firewall.allowedTCPPorts = [
    80 # http
    443 # https
    # 3306 # db
  ];

  # Oracle database drivers
  environment.unixODBCDrivers = with pkgs.unixODBCDrivers; [ msodbcsql17 ];

  # Can't serve websites from /home (use /var/www/)
  # And the website folder must be of owner "root" or current user
  services.httpd = {
    enable = true;
    extraModules = [ "headers" "rewrite" ];
    adminAddr = "admin@gmail.com";
    enablePHP = true;
    phpPackage = myPHP;
    virtualHosts.localhost = {
      documentRoot = "/var/www/cr-navarra-6x/public";
    };
    extraConfig = ''
      <Directory />
        DirectoryIndex index.php
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
      </Directory>
    '';
  };

  users.users.wwwrun.extraGroups = [ "wheel" ];

  # Default users don't have password, they're identified using unix_sockets.
  # In practice, with unix_sockets you're identified by the user you're running
  # the `mysql` command with.
  # To open mysql as the root user:
  # `sudo mysql`
  # Then you could create another user identified by password:
  # CREATE USER <new_user>@'localhost' IDENTIFIED BY '<new_user_password';
  # GRANT ALL ON <database_name>.* TO <new_user>@localhost;
  services.mysql = {
    enable = true;
    package = pkgs.mariadb;
  };

}
