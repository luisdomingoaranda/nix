{ config
, pkgs
, username
, home-manager
, ...
} @ inputs:

{

  imports = [
    home-manager.nixosModules.home-manager
  ];

  home-manager.users.${username} = {
    imports = [
      ../../home-manager/modules/kitty.nix
    ];
  };

  environment = {
    systemPackages = [ pkgs.kitty ];
    sessionVariables.TERMINAL = [ "kitty" ];
  };

}
