{ config
, pkgs
, username
, ...
} @ inputs:

{
  imports = [
    ./inadyn.nix
  ];

  environment.systemPackages = with pkgs; [
    ffmpeg-full
  ];

  security.acme.acceptTerms = true;
  security.acme.defaults.email = "luisdomingoaranda@gmail.com";

  services = {
    jellyfin = {
      enable = true;
      openFirewall = true;
    };

  };
}
