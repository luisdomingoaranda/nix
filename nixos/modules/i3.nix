{ config
, pkgs
, lib
, username
, nerdFont
, home-manager
, ...
} @ inputs:

{

  imports = [
    home-manager.nixosModules.home-manager
    ./pipewire.nix
  ];

  home-manager.users.${username} = {
    imports = [
      ../../home-manager/modules/polybar.nix
      ../../home-manager/modules/rofi.nix
    ];

    xdg.configFile = {
      i3.source = pkgs.fetchFromGitLab {
        owner = "luisdomingoaranda";
        repo = "i3";
        rev = "712b31e25a28b59ae56f03b8f500ec59f7de8023";
        hash = "sha256-/9Ti0Uo/Qxh/XdeKc0viFcjv4wCSuRVSUEDJJKUxj74=";
      };
    };

    xsession = {
      enable = true;
      profileExtra =
        let
          wallpaperPath = ../../assets/nix-wallpaper-nineish-dark-gray.png;
        in
        ''
          # Set screen resolution
          # If x preferred resolution is less than 1920, then set 1920 resolution
          x_pref=$(xrandr -q | grep -Pi "\+" | grep -Pio "^\s+\K(\d+)(?=x)")
          if [ $x_pref -le 1920 ]; then
            xrandr -s 1920x1080
          fi

          # Set wallpaper
          ${pkgs.feh}/bin/feh --bg-scale ${wallpaperPath}

          # Disable screensaver and autosuspend
          # Params:
          # s off (screensaver off)
          # -dpms (disables energy star features)
          ${pkgs.xorg.xset}/bin/xset s off -dpms
        '';
    };

    # Keybindings
    services.sxhkd = {
      enable = true;
      extraConfig = ''
        # Basics
        super + Return
          $TERMINAL

        # Scrot screenshots
        Print
          scrot -m $HOME/Pictures/%s_%H%M_%d.%m.%Y_$wx$h.png
        shift + @Print
          scrot -s $HOME/Pictures/%s_%H%M_%d%m%Y_$wx$h.png
        ctrl + shift + @Print
          scrot -s $HOME/Pictures/screenshot_tmp.png && xclip -t image/png -selection clipboard $HOME/Pictures/screenshot_tmp.png && rm $HOME/Pictures/screenshot_tmp.png

        # Volume controls
        control + shift + Up
          pamixer --increase 1
        control + shift + Down
          pamixer --decrease 1
      '';
    };

    # Append to .Xresources
    xresources.extraConfig = ''
      # Any folder name from .icons
      Xcursor.theme: Simp1e-Tokyo-Night
      Xft.dpi: 96
    '';
  };


  fonts.packages = with pkgs; [
    (pkgs.nerdfonts.override { fonts = [ nerdFont ]; })
    siji
  ];

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    layout = "us,es,de"; # us,es,ru,de,cn,cm
    xkbOptions = "grp:win_space_toggle,eurosign:e";
    autoRepeatDelay = 200;
    autoRepeatInterval = 25;
    displayManager = {
      defaultSession = "none+i3";
      autoLogin = {
        enable = true;
        user = username;
      };
      lightdm.enable = true;
    };
    windowManager.i3 = {
      enable = true;
      package = pkgs.i3;
      extraPackages = with pkgs; [
        dmenu # applicantion launcher
        feh # image viewer and set background img
        sxhkd # simple X hotkey daemon
        xorg.xev # identify keycodes in xorg
        i3status # status bar
        scrot # take screenshots
        xclip # clipboard
        xorg.xset # set user preferences for X
        xorg.xcursorthemes # cursor themes
        i3lock # lock window
      ];
    };
  };

  # Xorg compositor for transparency windows, etc. (with i3wm)
  services.picom = lib.mkMerge [
    {
      enable = true;
      opacityRules = [
        "99:class_g = 'kitty' && focused"
        "90:class_g = 'kitty' && !focused"
      ];
    }
    # # Don't allow blur effect for vmware virtual machine
    # (lib.mkIf (!config.virtualisation.vmware.guest.enable) {
    #   backend = "glx";
    #   vSync = true;
    #   settings = {
    #     blur = {
    #       background = true;
    #       method = "dual_kawase";
    #       strength = 5;
    #     };
    #   };
    #   fade = true;
    #   fadeDelta = 4;
    # })
  ];

}
