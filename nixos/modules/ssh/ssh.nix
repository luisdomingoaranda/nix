{ config
, username
, ...
} @ inputs:

let
  sshPubKeys = import ./sshPubKeys.nix;
  authorizedKeys = [
    sshPubKeys.pgp
  ];
in
{
  services.openssh = {
    enable = true;
    settings = {
      PasswordAuthentication = false;
      PermitRootLogin = "yes";
    };
    extraConfig = ''
      # Required for gpg agent forwarding
      StreamLocalBindUnlink yes
    '';
  };

  # Set allowed ssh keys for incoming connections
  # Root required for remote nixos-rebuild
  users.users = {
    ${username}.openssh.authorizedKeys.keys = authorizedKeys;
    root.openssh.authorizedKeys.keys = authorizedKeys;
  };
}
