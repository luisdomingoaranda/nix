{ config, lib, pkgs, ... }:
with lib;
let
  cfg = config.services.qbittorrent-nox;
  openFilesLimit = 4096;
  settingsFormat = pkgs.formats.ini {};
  settingsFile = settingsFormat.generate "qBittorrent.conf" cfg.settings;
in
{
  options.services.qbittorrent-nox = {
    enable = mkOption {
      type = types.bool;
      default = false;
      description = ''
        Run qBittorrent headlessly as systemwide daemon
      '';
    };

    package = mkPackageOption pkgs "qbittorrent-nox" {
      default = "qbittorrent-nox";
      example = "pkgs.qbittorrent-nox";
    };

    user = mkOption {
      type = types.str;
      default = "qbittorrent";
      description = ''
        User account under which qBittorrent runs.
      '';
    };

    group = mkOption {
      type = types.str;
      default = "qbittorrent";
      description = ''
        Group under which qBittorrent runs.
      '';
    };

    dataDir = mkOption {
      type = types.path;
      default = "/var/lib/qbittorrent";
      description = ''
        Directory where config and data files will be stored.
        Torrent files will be stored under `${dataDir}/qBittorrent/data/BT_backup`.
        Config file will be stored at `${cfg.dataDir}/qBittorrent/config/qBittorrent.conf`.
      '';
    };

    webUiPort = mkOption {
      type = lib.types.port;
      default = 8080;
      description = "The port the qBittorrent Web UI listens on.";
    };

    peerPort = mkOption {
      type = lib.types.port;
      default = 18959;
      description = ''
        The port qBittorrent will use to listen for incoming peer connections.
      '';
    };

    openWebUiFirewall = mkOption {
      type = types.bool;
      default = false;
      description = ''
        Open the webUiPort for incoming peer connections in the firewall.
      '';
    };

    openPeerFirewall = mkOption {
      type = types.bool;
      default = false;
      description = ''
        Open the peerPort for incoming peer connections in the firewall.
      '';
    };

    openFilesLimit = mkOption {
      default = openFilesLimit;
      description = ''
        Number of files to allow qBittorrent to open.
      '';
    };

    settings = mkOption {
      type = types.attrsOf (types.anything);
      description = ''
        Settings whose options overwrite fields in
        `${dataDir}/qBittorrent/config/qBittorrent.conf`
        (each time the service starts).

        See [qBittorrent's Wiki](https://github.com/qbittorrent/qBittorrent/wiki/Explanation-of-Options-in-qBittorrent)
        for documentation of settings.
      '';
      default = {
        Preferences = {
          "WebUI\\LocalHostAuth" = false;
          "WebUI\\Port" = 8080;
        };
        BitTorrent = {
          "Session\\Port" = 18959;
          "Session\\QueueingSystemEnabled" = false;
        };
      };
      example = {
        Application = {
          "FileLogger\\Age" = 7;
          "FileLogger\\MaxSizeBytes" = 66560;
          "FileLogger\\Enabled" = true;
          "FileLogger\\Path" = "var/lib/qbittorrent/qBittorrent/config/logs";
        };
        BitTorrent = {
          "Session\\Port" = 18959;
          "Session\\LSDEnabled" = false;
          "Session\\DHTEnabled" = false;
          "Session\\PeXEnabled" = false;
          "Session\\DisableAutoTMMByDefault" = false;
          "Session\\DisableAutoTMMTriggers\\CategorySavePathChanged" = false;
          "Session\\DisableAutoTMMTriggers\\DefaultSavePathChanged" = false;
        };
        Core = {
          "AutoDeleteAddedTorrentFile" = "Never";
        };
        Preferences = {
          "General\\Locale" = "en";
          "WebUI\\LocalHostAuth" = false;
          "WebUI\\Port" = 8080;
        };
        Network = {
          "PortForwardingEnabled" = false;
        };
      };
    };

  };

  config = mkIf cfg.enable {

    assertions = [
      # Ensure BitTorrent.Session\Port is set
      {
        assertion = cfg.settings.BitTorrent ? "Session\\Port";
        message = ''
          The `settings.BitTorrent."Session\\Port"` option must be set.
          Please specify a value for it in your configuration.
        '';
      }

      # Ensure Preferences.WebUI\Port is set
      {
        assertion = cfg.settings.Preferences ? "WebUI\\Port";
        message = ''
          The `settings.Preferences."WebUI\\Port"` option must be set.
          Please specify a value for it in your configuration.
        '';
      }

      # Consistency check for peerPort and BitTorrent.Session\Port
      {
        assertion = (! (cfg.settings.BitTorrent ? "Session\\Port")) ||
          (toString cfg.settings.BitTorrent."Session\\Port" == toString cfg.peerPort);
        message = ''
          The `peerPort` option (${toString cfg.peerPort}) must be the same as
          `settings.BitTorrent."Session\\Port"` (${toString cfg.settings.BitTorrent."Session\\Port"}).
          Please set them to the same value.
        '';
      }

      # Consistency check for webUiPort and Preferences.WebUI\Port
      {
        assertion = (! (cfg.settings.Preferences ? "WebUI\\Port")) ||
          (toString cfg.settings.Preferences."WebUI\\Port" == toString cfg.webUiPort);
        message = ''
          The `webUiPort` option (${toString cfg.webUiPort}) must be the same as
          `settings.Preferences."WebUI\\Port"` (${toString cfg.settings.Preferences."WebUI\\Port"}).
          Please set them to the same value.
        '';
      }
    ];

    environment.systemPackages = [ cfg.package ];

    # Open firewall TCP and UDP ports
    networking.firewall = {
      allowedUDPPorts = lib.mkIf cfg.openPeerFirewall [ cfg.peerPort ];
      allowedTCPPorts = []
        ++ lib.optionals cfg.openWebUiFirewall [
          cfg.webUiPort
        ] ++ lib.optionals cfg.openPeerFirewall [
          cfg.peerPort
        ];
    };

    systemd.services.qbittorrent = {
      after = [ "network-online.target" ];
      requires = [ "network-online.target" ];
      description = "qBittorrent Daemon";
      wantedBy = [ "multi-user.target" ];
      path = [ cfg.package ];
      serviceConfig = {

        # Service pre-start script: Create required directories
        ExecStartPre = [
          # Create the necessary directories before qBittorrent starts
          # Explicitly run these as root by adding `+` before the commands
          "+${pkgs.coreutils}/bin/mkdir -p ${cfg.dataDir}"
          "+${pkgs.coreutils}/bin/chown -R ${cfg.user}:${cfg.group} ${cfg.dataDir}"
          "+${pkgs.coreutils}/bin/chmod -R 775 ${cfg.dataDir}"
          # Copy settingsFile to appropiate location
          "+${pkgs.coreutils}/bin/cp ${settingsFile} ${cfg.dataDir}/qBittorrent/config/qBittorrent.conf"
        ];

        ExecStart = ''
          ${cfg.package}/bin/qbittorrent-nox \
            --profile=${cfg.dataDir}
        '';

        Restart = "on-failure";
        User = cfg.user;
        Group = cfg.group;
        UMask = "0002";
        LimitNOFILE = cfg.openFilesLimit;
      };
    };

    users.users = mkIf (cfg.user == "qbittorrent") {
      qbittorrent = {
        group = cfg.group;
        home = cfg.dataDir;
        createHome = true;
        description = "qBittorrent Daemon user";
      };
    };

    users.groups =
      mkIf (cfg.group == "qbittorrent") { qbittorrent = { gid = null; }; };
  };
}
