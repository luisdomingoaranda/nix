{ config
, pkgs
, username
, ...
} @ inputs:

let
  nextCloudPassFile = (builtins.toString ../../secrets/rpi4) + "/nextCloudAdmin.pass";
  nextCloudDomain = "luisdomingoaranda.com";
in
{
  imports = [
    ./inadyn.nix
  ];

  security.acme.acceptTerms = true;
  security.acme.defaults.email = "luisdomingoaranda@gmail.com";

  services = {
    nextcloud = {
      enable = true;
      package = pkgs.nextcloud29;
      hostName = nextCloudDomain;
      config.adminpassFile = nextCloudPassFile;
      maxUploadSize = "50G";
      https = true;
    };

    nginx.virtualHosts.${config.services.nextcloud.hostName} = {
      forceSSL = true;
      enableACME = true;
    };
  };
}
