{ config
, pkgs
, username
, ...
} @ inputs:

let
  domain = "nubedeazucar.cfd";
  email = "luisdomingoaranda@gmail.com";
in
{
  imports = [
    ./inadyn.nix
  ];

  # Open the necessary firewall ports for HTTP and HTTPS
  networking.firewall.allowedTCPPorts = [
    80   # HTTP
    443  # HTTPS
  ];

  security.acme = {
    acceptTerms = true;
    defaults.email = email;
  };

  services.nginx = {
    enable = true;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;

    virtualHosts = let
      SSL = {
        enableACME = true;  # Enable Let's Encrypt SSL certificates
        forceSSL = true;  # Redirect HTTP to HTTPS
      };
    in {
      # Virtual host for qbittorrent
      "qbittorrent.${domain}" = (SSL // {
        locations."/" = {
          proxyPass = "http://127.0.0.1:9090/";
        };
      });

      # Virtual host for radarr
      "radarr.${domain}" = (SSL // {
        locations."/" = {
          proxyPass = "http://127.0.0.1:7878/";
        };
      });

      # Virtual host for sonarr
      "sonarr.${domain}" = (SSL // {
        locations."/" = {
          proxyPass = "http://127.0.0.1:8989/";
        };
      });

      # Virtual host for readarr
      "readarr.${domain}" = (SSL // {
        locations."/" = {
          proxyPass = "http://127.0.0.1:8787/";
        };
      });

      # Virtual host for lidarr
      "lidarr.${domain}" = (SSL // {
        locations."/" = {
          proxyPass = "http://127.0.0.1:8686/";
        };
      });

      # Virtual host for calibre-server
      "calibre-web.${domain}" = (SSL // {
        locations."/" = {
          proxyPass = "http://127.0.0.1:8083/";
        };
      });

      # Virtual host for prowlarr
      "prowlarr.${domain}" = (SSL // {
        locations."/" = {
          proxyPass = "http://127.0.0.1:9696/";
        };
      });

      # Virtual host for jellyfin
      "jellyfin.${domain}" = (SSL // {
        locations."/" = {
          proxyPass = "http://127.0.0.1:8096/";
          proxyWebsockets = true; # to enable syncplay
        };
      });

      # Virtual host for jellyseerr
      "jellyseerr.${domain}" = (SSL // {
        locations."/" = {
          proxyPass = "http://127.0.0.1:5055/";
        };
      });

      # Virtual host for navidrome
      "navidrome.${domain}" = (SSL // {
        locations."/" = {
          proxyPass = "http://127.0.0.1:4533/";
        };
      });

      # Virtual host for immich
      "immich.${domain}" = (SSL // {
        locations."/" = {
          proxyPass = "http://[::1]:${toString config.services.immich.port}";
          proxyWebsockets = true;
          recommendedProxySettings = true;
          extraConfig = ''
            client_max_body_size 50000M;
            proxy_read_timeout   600s;
            proxy_send_timeout   600s;
            send_timeout         600s;
          '';
        };
      });

      # Virtual host for nextcloud
      "nextcloud.${domain}" = SSL;
    };

  };

}
