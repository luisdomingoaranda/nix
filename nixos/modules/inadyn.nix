{ config
, pkgs
, lib
, ...
} @ inputs:

let
  dynuPass = builtins.readFile ../../secrets/rpi4/ddclient.pass;
  portfolioDomain = "luisdomingoaranda.com";
  arrDomain = "nubedeazucar.cfd";
in
{
  services.inadyn = {
    enable = true;
    settings = {
      custom = {
        dyn = {
          username = "propet";
          password = dynuPass;
          hostname = [ portfolioDomain arrDomain ];
          ddns-server = "api.dynu.com";
          ddns-path = "/nic/update?hostname=%h&myip=%i&myipv6=no&username=%u&password=%p";
        };
      };
    };
  };
}
