{ config
, username
, pkgs
, ...
} @ inputs:

{
  users.users.${username}.shell = pkgs.zsh;
  programs.zsh.enable = true;
}
