{ config
, pkgs
, lib
, ...
} @ inputs:
{
  programs.steam = {
    enable = true;
    gamescopeSession.enable = true;
  };
  programs.gamemode.enable = true; # in steam: gamemoderun %command%, or gamemoderun mangohud %command%

  # Enable graphics acceleration
  hardware = {
    graphics = {
      enable = true;
      enable32Bit = true;
      extraPackages = with pkgs; [
        libvdpau-va-gl
      ];
    };
  };

  # Game controllers
  services.udev = {
    packages = with pkgs; [
      game-devices-udev-rules
    ];
  };
  hardware.uinput.enable = true;

  environment.systemPackages = with pkgs; [
    moonlight-qt # client for sunshine gaming streaming server
    lutris # Open Source gaming platform for GNU/Linux
    bottles # Easy-to-use wineprefix manager
    heroic # Native GOG, Epic, and Amazon Games Launcher for Linux, Windows and Mac
    protonup-qt # Install proton versions
    mangohud # Vulkan and OpenGL overlay for monitoring FPS, temperatures, CPU/GPU load and more
    vulkan-tools # for "vulkan-info" command
    mesa-demos # e.g.: glxgears
    pciutils # run: lspci -k
    libva-utils # run: vainfo
    lact # Linux AMDGPU Controller
    steam-rom-manager  # import roms to steam
    pcsx2 # ps2 emulator
    retroarchFull # retroarch with cores
  ];

}
