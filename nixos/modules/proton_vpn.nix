{ config
, pkgs
, username
, home-manager
, ...
} @ inputs:

{
  # Avoid leaking ipv6 address
  boot.kernelParams = ["ipv6.disable=1"];
  networking.enableIPv6 = false;  # avoid ip leaking with a vpn

  # Obtained from proton wireguard config file
  # From proton settings
  networking.firewall = {
    allowedUDPPorts = [ 51820 ]; # Open vpn port
  };
  networking.wg-quick.interfaces = {
    protonvpn = {
      autostart = true;
      address = [ "10.2.0.2/32" ];
      privateKeyFile = "/etc/wireguard/protonvpn.key";
      dns = [ "10.2.0.1" ];

      peers = [
        {
          publicKey = "OJv7H1H34Og2BqBwrUdTQPXZPD0oG1rh7wkMWAgGUAQ=";
          allowedIPs = [ "0.0.0.0/0" ];
          endpoint = "103.69.224.5:51820";
        }
      ];
    };
  };

}
