{ config
, pkgs
, ...
} @ inputs:

{

  environment = {
    systemPackages = [
      pkgs.gnome.nautilus
      # pkgs.nautilus-open-any-terminal # still doesn't work?
    ];
  };

  # Gnome virtual file system
  # Required for nautilus to work properly if using without GNOME
  services.gvfs.enable = true;

}
