{ config
, pkgs
, lib
, username
, nixpkgs-unstable
, ...
} @ inputs:
let
  pkgs-unstable = import nixpkgs-unstable { system = inputs.system; };
  dataDir = "/data";
  mediaGroup = "media";
  torrenterUser = "torrenter";
  streamerUser = "streamer";
  nextCloudPassFile = (builtins.toString ../../secrets/rpi4) + "/nextCloudAdmin.pass";
  radarrPort = 7878;
  radarrUser = "radarr";
  sonarrPort = 8989;
  sonarrUser = "sonarr";
  readarrPort = 8787;
  readarrUser = "readarr";
  lidarrPort = 8686;
  lidarrUser = "lidarr";
  jellyfinPort = 8096;
  jellyfinUser = "jellyfin";
  calibreUser = "calibre-server";
  calibeWebUser = "calibre-web";
  calibrePort = 5680;
  calibreWebPort = 8083;
  calibreLibraryDir = "${dataDir}/media/calibre";
  jellyseerrPort = 5055;
  prowlarrPort = 9696;
  immichUser = "immich";
  immichPort = 2285;
  immichDir = "${dataDir}/media/immich";
  qbittorrentWebPort = 9090;
  qbittorrentPeerPort = 18959;
  navidromePort = 4533;
  downloadDir = "${dataDir}/torrents";
  musicDir = "${dataDir}/media/music";
  nextcloudDir = "${dataDir}/media/nextcloud";
  nextcloudUser = "nextcloud";
in
{
  imports = [
    ./inadyn.nix
    ./qbittorrent_options.nix
    ./reverse_proxy.nix
  ];

  environment.systemPackages = with pkgs; [
    ffmpeg-full
    xteve # M3U Proxy for jellyfin
  ];

  security.acme.acceptTerms = true;
  security.acme.defaults.email = "luisdomingoaranda@gmail.com";

  # Set up the users/groups
  users.groups = {
    ${streamerUser} = {};
    ${torrenterUser} = {};
    ${mediaGroup} = {
      members = [
        radarrUser
        sonarrUser
        readarrUser
        lidarrUser
        calibreUser
        calibeWebUser
        jellyfinUser
        nextcloudUser
        immichUser
        username
      ];
    };
  };

  users.users = {
    ${streamerUser} = {
      isSystemUser = true;
      group = streamerUser;
    };
    ${torrenterUser} = {
      isSystemUser = true;
      group = torrenterUser;
    };
  };

  # Create the directories that the services will need with the correct permissions
  systemd.tmpfiles.rules = [
    "d ${dataDir}/media 0775 ${streamerUser} ${mediaGroup} -"
    "d ${dataDir}/media/movies 0775 ${streamerUser} ${mediaGroup} -"
    "d ${dataDir}/media/shows 0775 ${streamerUser} ${mediaGroup} -"
    "d ${dataDir}/media/calibre 0775 ${streamerUser} ${mediaGroup} -"
    "d ${dataDir}/media/games 0775 ${streamerUser} ${mediaGroup} -"
    "d ${dataDir}/media/music 0775 ${streamerUser} ${mediaGroup} -"
    "d ${dataDir}/media/my_music 0775 ${streamerUser} ${mediaGroup} -"
    "d ${dataDir}/media/subtitles 0775 ${streamerUser} ${mediaGroup} -"
    "d ${nextcloudDir} 0775 nextcloud nextcloud -"
    "d ${immichDir} 0775 ${immichUser} ${mediaGroup} -"
    "d ${immichDir}/backups 0775 ${immichUser} ${mediaGroup} -"
    "d ${immichDir}/encoded-video 0775 ${immichUser} ${mediaGroup} -"
    "d ${immichDir}/library 0775 ${immichUser} ${mediaGroup} -"
    "d ${immichDir}/profile 0775 ${immichUser} ${mediaGroup} -"
    "d ${immichDir}/thumbs 0775 ${immichUser} ${mediaGroup} -"
    "d ${immichDir}/upload 0775 ${immichUser} ${mediaGroup} -"
    "d ${dataDir}/torrents 0775 ${torrenterUser} ${mediaGroup} -"
    "d ${dataDir}/torrents/.incomplete 0775 ${torrenterUser} ${mediaGroup} -"
    "d ${dataDir}/torrents/.watch 0775 ${torrenterUser} ${mediaGroup} -"
    "d ${dataDir}/torrents/movies 0775 ${torrenterUser} ${mediaGroup} -"
    "d ${dataDir}/torrents/shows 0775 ${torrenterUser} ${mediaGroup} -"
    "d ${dataDir}/torrents/games 0775 ${torrenterUser} ${mediaGroup} -"
    "d ${dataDir}/torrents/music 0775 ${torrenterUser} ${mediaGroup} -"
    "d ${dataDir}/torrents/apps 0775 ${torrenterUser} ${mediaGroup} -"
    "d ${dataDir}/torrents/sports 0775 ${torrenterUser} ${mediaGroup} -"
    "d ${dataDir}/torrents/subtitles 0775 ${torrenterUser} ${mediaGroup} -"
    "d ${dataDir}/torrents/books 0775 ${torrenterUser} ${mediaGroup} -"
    "d ${dataDir}/torrents/audiobooks 0775 ${torrenterUser} ${mediaGroup} -"
    # "d ${dataDir}/usenet 0755 usenet ${mediaGroup} -"
    # "d ${dataDir}/usenet/.incomplete 0755 usenet ${mediaGroup} -"
    # "d ${dataDir}/usenet/.watch 0755 usenet ${mediaGroup} -"
    # "d ${dataDir}/usenet/manual 0775 usenet ${mediaGroup} -"
    # "d ${dataDir}/usenet/complete/movies 0775 usenet ${mediaGroup} -"
    # "d ${dataDir}/usenet/complete/shows 0775 usenet ${mediaGroup} -"
    # "d ${dataDir}/usenet/complete/books 0775 usenet ${mediaGroup} -"
  ];

  # Set up the services
  services.radarr = {
    enable = true;
    group = mediaGroup;
    user = radarrUser;
  };

  services.sonarr= {
    enable = true;
    group = mediaGroup;
    user = sonarrUser;
  };

  # services.sabnzbd = {
  #   enable = true;
  #   user = "usenet";
  #   group = "media";
  # };

  services.jellyfin = {
    enable = true;
    group = mediaGroup;
    user = jellyfinUser;
    openFirewall = true;
  };

  services.navidrome = {
    enable = true;
    group = mediaGroup;
    settings = {
      MusicFolder = musicDir;
      Port = navidromePort;
    };
  };

  services.prowlarr = {
    enable = true;
    package = pkgs-unstable.prowlarr;
  };

  services.readarr = {
    enable = true;
    group = mediaGroup;
    user = readarrUser;
  };

  services.lidarr= {
    enable = true;
    group = mediaGroup;
    user = lidarrUser;
  };

  services.calibre-server = {
    enable = true;
    group = mediaGroup;
    user = calibreUser;
    port = calibrePort;
    libraries = [
      # Create it first with calibre GUI
      calibreLibraryDir
    ];
    # Create new users database for settings username and passwords:
    # calibre-server --userdb /data/media/calibre/users.sqlite --manage-users
    auth = {
      enable = true;
      mode = "basic";
      userDb = "${calibreLibraryDir}/users.sqlite";
    };
  };

  services.calibre-web = {
    # Never edit or add books through calibre-web
    # User plain calibre instead
    #
    # Default user: admin
    # Default pass: admin123
    # Add users and change passwords from the webpage admin panel
    enable = true;
    group = mediaGroup;
    user = calibeWebUser;
    options = {
      calibreLibrary = calibreLibraryDir;
      reverseProxyAuth.enable = true;
      enableBookUploading = false;
    };
    listen = {
      ip = "127.0.0.1";
      port = calibreWebPort;
    };
  };

  services.jellyseerr = {
    enable = true;
    port = jellyseerrPort;
  };

  services.nextcloud = {
    enable = true;
    hostName = "nextcloud.nubedeazucar.cfd";
    https = true;
    package = pkgs.nextcloud30;
    config.adminpassFile = nextCloudPassFile;
    maxUploadSize = "200G";
    extraApps = {
      inherit (config.services.nextcloud.package.packages.apps) calendar tasks;
    };
    extraAppsEnable = true;
    config = {
      dbtype = "sqlite";
    };
  };

  services.immich = {
    enable = true;
    port = immichPort;
    user = immichUser;
    group = mediaGroup;
    mediaLocation = immichDir;
  };

  services.qbittorrent-nox = {
    enable = true;
    package = pkgs.qbittorrent-nox;
    user = torrenterUser;
    group = mediaGroup;
    dataDir = "/var/lib/qbittorrent";
    webUiPort = qbittorrentWebPort;
    peerPort = qbittorrentPeerPort;
    openPeerFirewall = true;
    settings = {
      BitTorrent = {
        "Session\\Port" = qbittorrentPeerPort;
        "Session\\DefaultSavePath" = downloadDir;
        "Session\\LSDEnabled" = false;
        "Session\\DHTEnabled" = false;
        "Session\\PeXEnabled" = false;
        "Session\\QueueingSystemEnabled" = false;
        "Session\\DisableAutoTMMByDefault" = false;
        "Session\\DisableAutoTMMTriggers\\CategorySavePathChanged" = false;
        "Session\\DisableAutoTMMTriggers\\DefaultSavePathChanged" = false;
      };
      Preferences = {
        "WebUI\\LocalHostAuth" = false;
        "WebUI\\Port" = qbittorrentWebPort;
      };
      Network = {
        "PortForwardingEnabled" = false;
      };
    };
  };

  services.samba = {
    enable = true;
    openFirewall = true;
    settings = {
      global = {
        security = "user";
        "map to guest" = "Bad User"; # Allows unauthenticated connections to be mapped to guest
      };
      public = {
        path = dataDir;
        browseable = "yes";
        comment = "Public samba share.";
        "guest ok" = "yes";
        "read only" = "yes";
      };
    };
  };

  # Allows autodiscovery from client network tab
  services.samba-wsdd = {
    enable = true;
    openFirewall = true;
  };

  # NFS
  services.nfs.server = {
    enable = true;
    exports = ''
      /data *(rw,async,no_subtree_check,no_root_squash)
    '';
  };
  networking.firewall.allowedTCPPorts = [ 2049 ]; # allow NFS port

}
