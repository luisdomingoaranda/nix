{ config
, pkgs
, username
, home-manager
, ...
} @ inputs:

{

  imports = [
    home-manager.nixosModules.home-manager
  ];

  home-manager.users.${username} = {
    xdg.configFile.alacritty.source = pkgs.fetchFromGitLab {
      owner = "luisdomingoaranda";
      repo = "alacritty";
      rev = "7e5884c481bbe516d6558a4ff39ef1b08dceef09";
      sha256 = "sha256-jYXZXqE4nxWHYgq2bzqB99G7Z6TkthxbzfPimmdO02E=";
    };
  };

  environment = {
    systemPackages = [ pkgs.alacritty ];
    sessionVariables.TERMINAL = [ "alacritty" ];
  };

}
