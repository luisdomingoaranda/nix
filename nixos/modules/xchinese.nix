{ config
, pkgs
, lib
, username
, home-manager
, ...
} @ inputs:

{

  imports = [
    home-manager.nixosModules.home-manager
  ];

  home-manager.users.${username}.xsession = {
    enable = true;
    profileExtra = ''
      # Launch ibus-daemon
      ibus-daemon &
    '';
  };

  # Allow chinese pinyin
  i18n.inputMethod = {
    enabled = "ibus";
    ibus.engines = with pkgs.ibus-engines; [ libpinyin ];
  };

  fonts.packages = with pkgs; [
    wqy_microhei # chinese fonts
  ];

}
