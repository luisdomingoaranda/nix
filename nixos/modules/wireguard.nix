{ config
, pkgs
, lib
, username
, ...
} @ inputs:
let
  vpnServerPort = 51820;
in
{
  networking.firewall = {
    allowedUDPPorts = [ vpnServerPort ];
  };

  networking.wg-quick.interfaces = {
    proton0-es-33 = {
      address = [ "10.2.0.2/32" ];
      dns = [ "10.2.0.1" ];
      privateKey = "QNA/hnuWz3bzmkJOTWz2jQ1cEOkfCWLnc+zk/XpsNmg=";

      peers = [
        {
          publicKey = "roOsz9dJeKKVt6E3EIEKXQfZsmhSfsqOceZWiuGLIgg=";
          allowedIPs = [ "0.0.0.0/0" ];
          endpoint = "185.76.11.17:${toString vpnServerPort}";
          persistentKeepalive = 25;
        }
      ];
    };
  };
}
