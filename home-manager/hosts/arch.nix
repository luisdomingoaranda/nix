{ pkgs
, config
, lib
, stdenv
, ...
} @ inputs:

let
  myPkgs = inputs.mynixpkgs.packages.${inputs.system};
in
{
  imports = [
    ../modules/common.nix
    # ../modules/hyprland.nix
    # ../modules/sway.nix
    # ../modules/i3.nix
    ../modules/kitty.nix
    ../modules/neovim.nix
    ../modules/nix-tools.nix
    ../modules/zsh.nix
    ../modules/fzf.nix
    ../modules/tmux.nix
    ../modules/git.nix
    ../modules/browsers.nix
    ../modules/ssh.nix
    ../modules/yubikey.nix
    ../modules/embedded.nix
    ../modules/nixgl.nix
  ];

  fonts.fontconfig.enable = true;
  targets.genericLinux.enable = true;

  home = {
    username = inputs.username;
    homeDirectory = "/home/${inputs.username}";
    stateVersion = "22.05";
    packages = with pkgs; [
      myPkgs.sentences2anki # Create anki notes from sentences
      texliveFull # tex live environment
      texstudio # TeX and LaTeX editor
      anki
      sqlitebrowser # DB Browser for SQLite

      # Fonts
      (nerdfonts.override { fonts = [ inputs.nerdFont ]; })
      noto-fonts-emoji
      siji

      # GUI
      teams-for-linux
      peek # GIF screen recorder
      # openmodelica.combined # simulink and simscape like open software
      chromium

      # Development
      qalculate-gtk # calculator
      hexyl # command-line hex viewer
      unixtools.xxd # hex-editor
      gnumake # make
      filezilla # ftp/sftp client (GUI)
      remmina # remote desktop client
      inputs.openconnect-sso.packages.${inputs.system}.default # vpn client (annyconnect compatible)
      # strongswan # vpn client (ipsec based)
      openfortivpn # vpn client
      dbeaver-bin # sql client
      postman # API development environment

      # Media
      transmission_4-gtk # torrents
      nemo # file browser
      ffmpeg # record, convert and stream audio and video
      feh # image viewer and set wallpaper
      eog # gnome image viewer
      zathura # pdf reader (GUI)
      vlc # media player
      mpv # general purpose media player
      evince # document viewer

      # Creation / editing tools
      libreoffice # office
      gimp # image edition
      inkscape # vector graphics (GUI)

      # Command line
      exfat # Free exFAT file system implementation
      util-linux # set of system utilities for linux
      xterm # fallback terminal
      wget # Tool for retrieving files using HTTP, HTTPS, and FTP
      git-crypt # Transparent file encryption in git
      screen # A window manager that multiplexes a physical terminal
      xmlstarlet # A command line tool for manipulating and querying XML data
      ecmtools # uncompress ECM files to bin cd format (ecm-uncompress command)
      entr # run arbitrary commands when a file changes
      # bear # generate compile_comamnds.json
      jq # command-line JSON processor
      p7zip # Extract 7zip files
      zip # Extract
      unzip # Extract
      unrar # Extract
      woeusb # create bootable usb with windows 10 iso
      file # which type a file is
      usbutils # tools for working with usb devices, such as lsusb
      ripgrep # mixture of grep and Ag (silver searcher)
      htop # process viewer
      pandoc # Conversion between markup formats
      graphviz # Graph visualization tools (such as dot command)
      tree # Depth idented directory visualization

      # Networking
      traceroute
      nettools
      unixtools.netstat

      # security
      sqlmap # Automatic SQL injection and database takeover tool
      wireshark # sniffer
      nmap # network discovery
      exiftool # read and edit files metadata
      whatweb # website scanner: get technology stack
      john # John the Ripper password cracker

      # Ricing
      lxappearance # set gtk theme
      libsForQt5.qt5ct # set qt theme
      font-manager
      neofetch

      # GPU
      nvtopPackages.full
      clinfo
      gpu-viewer
      vulkan-tools
    ];
  };
}
