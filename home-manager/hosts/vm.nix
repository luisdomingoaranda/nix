{ pkgs
, config
, lib
, stdenv
, ...
} @ inputs:

let
  myPkgs = inputs.mynixpkgs.packages.${inputs.system};
in
{
  imports = [
    ../modules/common.nix
    ../modules/neovim.nix
    ../modules/nix-tools.nix
    ../modules/zsh.nix
    ../modules/fzf.nix
    ../modules/tmux.nix
    ../modules/git.nix
    ../modules/gui-theme.nix
    ../modules/browsers.nix
    ../modules/ssh.nix
  ];

  home = {
    username = inputs.username;
    homeDirectory = "/home/${inputs.username}";
    stateVersion = "22.05";
    packages = with pkgs; [
      # My packages
      myPkgs.vpn_rsi
      # myNixpkgs.sentences2anki # Create anki notes from sentences

      # Development
      qalculate-gtk # calculator
      hexyl # command-line hex viewer
      unixtools.xxd # hex-editor
      gcc # C compiler
      gnumake # make
      filezilla # ftp/sftp client (GUI)
      remmina # remote desktop client
      openconnect # vpn client (annyconnect compatible)
      dbeaver-bin # sql client
      postman # API development environment

      # Media
      transmission_4-gtk # torrents
      ffmpeg # record, convert and stream audio and video
      feh # image viewer and set wallpaper
      zathura # pdf reader (GUI)

      # Creation / editing tools
      libreoffice # office
      gimp # image edition

      # Command line
      git-crypt # Transparent file encryption in git
      screen # A window manager that multiplexes a physical terminal
      xmlstarlet # A command line tool for manipulating and querying XML data
      ecmtools # uncompress ECM files to bin cd format (ecm-uncompress command)
      entr # run arbitrary commands when a file changes
      # bear # generate compile_comamnds.json
      jq # command-line JSON processor
      p7zip # Extract 7zip files
      zip
      unzip # Extract
      unrar # Extract
      woeusb # create bootable usb with windows 10 iso
      file # which type a file is
      usbutils # tools for working with usb devices, such as lsusb
      ripgrep # mixture of grep and Ag (silver searcher)
      htop # process viewer
      pandoc # Conversion between markup formats
      graphviz # Graph visualization tools (such as dot command)
      tree # Depth idented directory visualization

      # Networking
      traceroute
      nmap
      nettools
      unixtools.netstat

      # Ricing
      lxappearance
      neofetch
    ];
  };
}
