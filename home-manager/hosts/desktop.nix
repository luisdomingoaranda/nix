{ pkgs
, config
, lib
, stdenv
, ...
} @ inputs:

let
  myPkgs = inputs.mynixpkgs.packages.${inputs.system};
in
{
  imports = [
    ../modules/common.nix
    ../modules/neovim.nix
    ../modules/nix-tools.nix
    ../modules/zsh.nix
    ../modules/fzf.nix
    ../modules/tmux.nix
    ../modules/git.nix
    ../modules/latex.nix
    ../modules/embedded.nix
    ../modules/gui-theme.nix
    ../modules/browsers.nix
    ../modules/ssh.nix
  ];

  home = {
    username = inputs.username;
    homeDirectory = "/home/${inputs.username}";
    stateVersion = "22.05";
    packages = with pkgs; [
      # My packages
      myPkgs.vpn_rsi
      myPkgs.whatismyip
      myPkgs.sentences2anki # Create anki notes from sentences

      # Apps                    # notification daemon
      teams # MS teams
      logisim-evolution # digital logic designer and simulator

      # Development
      qalculate-gtk # calculator
      hexyl # command-line hex viewer
      unixtools.xxd # hex-editor
      arduino # arduino software with compilers (e.g. avr-gcc)
      gcc # C compiler
      vscode # vistual studio code editor
      gnumake # make
      filezilla # ftp/sftp client (GUI)
      remmina # remote desktop client
      openfortivpn # vpn client
      openconnect # vpn client (annyconnect compatible)
      dbeaver-bin # sql client
      postman # API development environment

      # Media
      transmission_4-gtk # torrents
      ffmpeg # record, convert and stream audio and video
      feh # image viewer and set wallpaper
      nomacs # image viewer
      # peek # gif recorder
      vlc # music, video player
      mplayer # video player
      smplayer # frontend for mplayer
      eog # gnome image viewer
      zathura # pdf reader (GUI)
      calibre # epub reader
      shotwell # image/photo manager/organizer

      # Study
      mpv
      anki

      # Creation / editing tools
      libreoffice # office
      # blender # 3D (GUI)
      inkscape # vector graphics (GUI)
      gimp # free photoshop (GUI)
      kicad # electronics design automation suite

      # Command line
      exfat # Free exFAT file system implementation
      exfatprogs # exFAT filesystem userspace utilities
      git-crypt # Transparent file encryption in git
      screen # A window manager that multiplexes a physical terminal
      ecmtools # uncompress ECM files to bin cd format (ecm-uncompress command)
      entr # run arbitrary commands when a file changes
      bear # generate compile_comamnds.json
      jq # command-line JSON processor
      p7zip # Extract 7zip files
      zip
      unzip # Extract
      unrar # Extract
      woeusb # create bootable usb with windows 10 iso
      file # which type a file is
      screen # serial terminal
      man-pages # linux man pages
      stdmanpages # C++ man pages
      usbutils # tools for working with usb devices, such as lsusb
      ripgrep # mixture of grep and Ag (silver searcher)
      htop # process viewer
      pandoc # Conversion between markup formats
      graphviz # Graph visualization tools (such as dot command)
      tree # Depth idented directory visualization

      # Networking
      traceroute
      nmap
      nettools
      unixtools.netstat

      # Ricing / useless
      lxappearance # gtk theme picker
      t-rec # terminal recording
      font-manager
      neofetch
      figlet
      cowsay
      fortune
      cmatrix
      pipes
    ];
  };
}
