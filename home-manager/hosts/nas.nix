{ pkgs
, config
, lib
, stdenv
, nixpkgs-stable
, ...
} @ inputs:

let
  myPkgs = inputs.mynixpkgs.packages.${inputs.system};
  pkgs-stable = import nixpkgs-stable { system = inputs.system; };
in
{
  imports = [
    ../modules/common.nix
    ../modules/neovim.nix
    ../modules/nix-tools.nix
    ../modules/zsh.nix
    ../modules/fzf.nix
    ../modules/tmux.nix
    ../modules/git.nix
    ../modules/latex.nix
    ../modules/embedded.nix
    ../modules/browsers.nix
    ../modules/ssh.nix
  ];

  home = {
    username = inputs.username;
    homeDirectory = "/home/${inputs.username}";
    stateVersion = "24.11";
    packages = with pkgs; [
      # My packages
      myPkgs.vpn_rsi
      myPkgs.whatismyip
      # myPkgs.sentences2anki # Create anki notes from sentences

      # Apps
      # teams # MS teams
      # firefox
      google-chrome
      element-desktop # Matrix protocol clients
      hexchat # IRC graphical client
      kodi # media center
      jellyfin-media-player # jellyfin desktop app

      # Development
      qalculate-gtk # calculator
      hexyl # command-line hex viewer
      unixtools.xxd # hex-editor
      arduino # arduino software with compilers (e.g. avr-gcc)
      gcc # C compiler
      vscode # vistual studio code editor
      gnumake # make
      filezilla # ftp/sftp client (GUI)
      remmina # remote desktop client
      openfortivpn # vpn client
      openconnect # vpn client (annyconnect compatible)
      dbeaver-bin # sql client
      postman # API development environment

      # Media
      yt-dlp # Command-line tool to download videos from YouTube.com and other sites (youtube-dl fork)
      davinci-resolve # Professional video editing, color, effects and audio post-processing
      tesseract # OCR engine
      vobsub2srt # Converts VobSub subtitles into SRT subtitles
      subtitleedit # gui for generating subtitles
      picard # Official MusicBrainz tagger
      kid3 # Simple and powerful audio tag editor
      easytag # View and edit tags for various audio files
      puddletag # Audio tag editor similar to the Windows program, Mp3tag
      mkvtoolnix # Cross-platform tools for Matroska
      makemkv # Convert blu-ray and dvd to mkv
      mediainfo # Supplies technical and tag information about a video or audio file
      qbittorrent # torrent client
      qbittorrent-nox # torrent client headless for web-version
      smartmontools # Tools for monitoring the health of hard drives (e.g. smartctl)
      ffmpeg-full # record, convert and stream audio and video
      feh # image viewer and set wallpaper
      nomacs # image viewer
      # peek # gif recorder
      vlc # music, video player
      mplayer # video player
      smplayer # frontend for mplayer
      eog # gnome image viewer
      zathura # pdf reader (GUI)
      pkgs-stable.calibre # epub reader
      shotwell # image/photo manager/organizer

      # Study
      mpv
      pkgs-stable.anki

      # Creation / editing tools
      libreoffice # office
      # blender # 3D (GUI)
      inkscape # vector graphics (GUI)
      gimp # free photoshop (GUI)
      # kicad # electronics design automation suite

      # Command line
      # exfat # Free exFAT file system implementation
      exfatprogs # exFAT filesystem userspace utilities
      git-crypt # Transparent file encryption in git
      screen # A window manager that multiplexes a physical terminal
      ecmtools # uncompress ECM files to bin cd format (ecm-uncompress command)
      entr # run arbitrary commands when a file changes
      # bear # generate compile_comamnds.json
      jq # command-line JSON processor
      p7zip # Extract 7zip files
      zip
      unzip # Extract
      unrar # Extract
      woeusb # create bootable usb with windows 10 iso
      file # which type a file is
      screen # serial terminal
      man-pages # linux man pages
      stdmanpages # C++ man pages
      usbutils # tools for working with usb devices, such as lsusb
      ripgrep # mixture of grep and Ag (silver searcher)
      htop # process viewer
      pandoc # Conversion between markup formats
      graphviz # Graph visualization tools (such as dot command)
      tree # Depth idented directory visualization

      # Networking
      traceroute
      nmap
      nettools
      unixtools.netstat

      # Ricing / useless
      lxappearance # gtk theme picker
      t-rec # terminal recording
      font-manager
      neofetch
      figlet
      cowsay
      fortune
      cmatrix
      pipes
    ];
  };
}
