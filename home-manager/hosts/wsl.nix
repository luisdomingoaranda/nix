{ pkgs
, config
, lib
, stdenv
, ...
} @ inputs:

let
  myPkgs = inputs.mynixpkgs.packages.${inputs.system};
in
{
  imports = [
    ../modules/common.nix
    ../modules/neovim.nix
    ../modules/nix-tools.nix
    ../modules/zsh.nix
    ../modules/fzf.nix
    ../modules/tmux.nix
    ../modules/git.nix
    ../modules/ssh.nix
    ../modules/yubikey.nix
  ];

  fonts.fontconfig.enable = true;

  home = {
    username = inputs.username;
    homeDirectory = "/home/${inputs.username}";
    stateVersion = "22.05";
    packages = with pkgs; [
      myPkgs.sentences2anki # Create anki notes from sentences

      # Development
      hexyl # command-line hex viewer
      unixtools.xxd # hex-editor
      gnumake # make

      # Media
      ffmpeg # record, convert and stream audio and video
      feh # image viewer and set wallpaper

      # Command line
      wget # Tool for retrieving files using HTTP, HTTPS, and FTP
      git-crypt # Transparent file encryption in git
      screen # A window manager that multiplexes a physical terminal
      xmlstarlet # A command line tool for manipulating and querying XML data
      ecmtools # uncompress ECM files to bin cd format (ecm-uncompress command)
      entr # run arbitrary commands when a file changes
      # bear # generate compile_comamnds.json
      jq # command-line JSON processor
      p7zip # Extract 7zip files
      zip # Extract
      unzip # Extract
      unrar # Extract
      file # which type a file is
      usbutils # tools for working with usb devices, such as lsusb
      ripgrep # mixture of grep and Ag (silver searcher)
      htop # process viewer
      pandoc # Conversion between markup formats
      tree # Depth idented directory visualization

      # Networking
      traceroute
      nettools
      unixtools.netstat

      # security
      sqlmap # Automatic SQL injection and database takeover tool
      wireshark # sniffer
      nmap # network discovery
      exiftool # read and edit files metadata
      whatweb # website scanner: get technology stack
      john # John the Ripper password cracker

      # Ricing
      neofetch
    ];
  };
}
