{ config
, pkgs
, ...
} @ inputs:
{
    home.packages = with pkgs; [
      nixgl.nixGLIntel
      nixgl.nixVulkanIntel
    ];
}
