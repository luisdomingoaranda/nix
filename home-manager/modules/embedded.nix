{ config
, pkgs
, ...
} @ inputs:
let
  myPkgs = inputs.mynixpkgs.packages.${inputs.system};
in
{
  nixpkgs.config.allowUnfree = true;

  home.packages = with pkgs; [
    kingstvis
    openocd

    # ARM
    gcc-arm-embedded # arm-none-eabi-{ar,as,ld,gcc,nm,objcopy,objdump,readelf,..}
    stm32cubemx
    stlink # st-info st-flash st-trace st-util
  ];
}
