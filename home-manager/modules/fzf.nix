{ config
, pkgs
, ...
} @ inputs:
{
  programs = {
    fzf = { enable = true; };
  };
}
