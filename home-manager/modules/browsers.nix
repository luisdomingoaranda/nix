{ config
, pkgs
, ...
} @ inputs:
let
  browser = "brave"; # supported: chromium, ungoogled-chromium, google-chrome, brave, vivaldi
in
{
  home.packages = [
    pkgs.${browser}
  ];

  programs.chromium = {
    enable = true;
    package = pkgs.${browser};
    extensions = [
      "nngceckbapebfimnlniiiahkandclblb" # bitwarden
      "dbepggeogbaibhgnhhndojpepiihcmeb" # vimium
      "cjpalhdlnbpafiamejdnhcphjbkeiagm" # ublock origin
      "nkbihfbeogaeaoehlefnkodbefgpgknn" # metamask
      "nhdogjmejiglipccpnnnanhbledajbpd" # vue.js devtools
      "fmkadmapgofadopljbjfkapdkoienihi" # react developer tools
      "nkkkaendbndanjjndfpebmekhgdjlhkh" # chatgpt latex copy
    ];
  };
}
