{ config
, pkgs
, ...
} @ inputs:
{
  home.packages = with pkgs; [
    pinentry-gtk2 # needed for PIN (curses gives problems)
    yubikey-personalization-gui # needs yubikey-personalization udev rules
    yubikey-manager # Command line tool for configuring any YubiKey over all USB transports
  ];

  programs.gpg = {
    enable = true;
    publicKeys = [
      {
        # yubikey pgp
        source = ../../pub.key;
        trust = 5; # ultimate
      }
    ];
  };

  services.gpg-agent = {
    enable = true;
    enableSshSupport = true;

    # "curses" option won't ask for PIN with certain programs (e.g. ssh)
    # It just fails.
    # A workaround to use "curses" as pinentryFlavor would be to first
    # encrypt some file with gpg, because that will succesfully launch the
    # curses prompt to ask you the PIN.
    # Afterwards use any other programs with the cached PIN.
    pinentryPackage = pkgs.pinentry-gtk2;
  };
}
