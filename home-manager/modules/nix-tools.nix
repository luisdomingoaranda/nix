{ config
, pkgs
, ...
} @ inputs:
{
  home.packages = with pkgs; [
    strace
    nodePackages.node2nix # nix generator
    nix-index # file database for nixpkgs
    manix # A Fast Documentation Searcher for Nix
    cntr # A container debugging tool based on FUSE
    # steam-run # run executables in a FHS
    nix-tree # see dependency graph of a derivation
  ];
}
