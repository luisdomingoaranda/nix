{ config
, pkgs
, ...
} @ inputs:
{
  imports = [
    # ./nixgl.nix
    ./colorscheme.nix
  ];

  # If got problems, and nixGl doesn't help,
  # install kitty by another package manager (e.g. pacman)
  # programs.kitty = {
  #   enable = true;
  #   package = pkgs.writeShellScriptBin "kitty" ''
  #     #!bin/sh
  #     ${pkgs.nixgl.nixGLIntel}/bin/nixGLIntel ${pkgs.kitty}/bin/kitty
  #   '';
  # };

  xdg.configFile."kitty/kitty.conf".text = ''
    font_family ${inputs.nerdFont} Nerd Font
    font_size 12.0
    background_opacity 0.9

    cursor #${config.colorScheme.palette.base05}
    foreground #${config.colorScheme.palette.base05}
    background #${config.colorScheme.palette.base00}
    selection_background #${config.colorScheme.palette.base05}
    selection_foreground #${config.colorScheme.palette.base00}
    url_color #${config.colorScheme.palette.base04}

    # normal
    color0 #${config.colorScheme.palette.base00}
    color1 #${config.colorScheme.palette.base08}
    color2 #${config.colorScheme.palette.base0B}
    color3 #${config.colorScheme.palette.base0A}
    color4 #${config.colorScheme.palette.base0D}
    color5 #${config.colorScheme.palette.base0E}
    color6 #${config.colorScheme.palette.base0C}
    color7 #${config.colorScheme.palette.base05}

    # bright
    color8 #${config.colorScheme.palette.base03}
    color9 #${config.colorScheme.palette.base09}
    color10 #${config.colorScheme.palette.base01}
    color11 #${config.colorScheme.palette.base02}
    color12 #${config.colorScheme.palette.base04}
    color13 #${config.colorScheme.palette.base06}
    color14 #${config.colorScheme.palette.base0F}
    color15 #${config.colorScheme.palette.base07}
  '';

  home.sessionVariables.TERMINAL = "kitty";
}
