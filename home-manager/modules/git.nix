{ config
, pkgs
, ...
} @ inputs:
{
  programs.git = {
    enable = true;
    package = pkgs.gitAndTools.gitFull;
    userName = "luisdomingoaranda";
    userEmail = "luisdomingoaranda@gmail.com";
    # extraConfig.core.editor = "vim";
    aliases = {
      l = "log --all --decorate --oneline --graph";
      s = "status";
      b = "log --follow -p --";
    };
  };

}
