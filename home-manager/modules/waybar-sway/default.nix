{ config
, pkgs
, lib
, username
, nerdFont
, base16scheme
, nix-colors
, ...
} @ inputs:

{
  imports = [
    ../colorscheme.nix
  ];

  home.packages = with pkgs; [
    waybar
  ];

  xdg.configFile."waybar/config".source = ./conf.json;
  xdg.configFile."waybar/style.css".source = ./style.css;

}
