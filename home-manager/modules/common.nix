{ config
, pkgs
, ...
} @ inputs:
{
  programs = {
    home-manager = { enable = true; };
  };
}
