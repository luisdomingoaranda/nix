{ config
, pkgs
, ...
} @ inputs:

let
  shellDir = ../shell;
in
{
  programs.zsh = {
    enable = true;
    shellAliases = import "${shellDir}/shellAliases.nix" { inherit pkgs; };
    sessionVariables = import "${shellDir}/sessionVariables.nix";
    enableCompletion = true;
    autosuggestion.enable = true;
    initExtra = ''
      source ${shellDir}/myTheme.zsh-theme;

      # Source scripts
      source ${shellDir}/shellFunctions.sh;
      for i in $(find ${shellDir}/rsi/*); do
        source $i;
      done

      # Ctrl+Space for autocompletion
      bindkey '^ ' autosuggest-accept

      # Avoid ssh_askpass gui app when asking for credentials
      unset SSH_ASKPASS
    '';
    oh-my-zsh = {
      enable = true;
      # theme = "flazz";  # "random" "agnoster" "pmcgee" "jonathan"
      plugins = [ "jsontools" ];
    };
  };

}
