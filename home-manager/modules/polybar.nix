{ config
, pkgs
, lib
, username
, nerdFont
, base16scheme
, nix-colors
, ...
} @ inputs:

let
  pipewire_module_script = pkgs.writeShellScript "pipewire.sh" ''
    PATH=${lib.makeBinPath [ pkgs.coreutils pkgs.pamixer pkgs.pipewire ]}
    VOLUME=$(pamixer --get-volume-human)
    case $1 in
        "up")
            pamixer --increase 10
            ;;
        "down")
            pamixer --decrease 10
            ;;
        "mute")
            pamixer --toggle-mute
            ;;
        *)
            echo "󰓃 ''${VOLUME}"
    esac
  '';
in
{
  imports = [
    ./colorscheme.nix
  ];

  home.packages = with pkgs; [
    pamixer
    pavucontrol
  ];

  services.polybar = {
    enable = true;
    # Polybar is launched from i3 config
    # Otherwise the i3 polybar module won't work
    # Probably polybar launches first and doesn't find i3 in PATH
    # But if not enabled, the config file won't be generated
    script = "";
    package = pkgs.polybar.override {
      i3Support = true;
    };
    settings = {
      settings = {
        screenchange.reload = "true";
      };
      "global/wm" = {
        margin.top = 5;
        margin.bottom = 5;
      };
      colors = {
        background = "#${config.colorScheme.palette.base00}";
        background-alt = "#${config.colorScheme.palette.base01}";
        foreground = "#${config.colorScheme.palette.base04}";
        foreground-alt = "#${config.colorScheme.palette.base05}";
        primary = "#${config.colorScheme.palette.base02}";
        secondary = "#b1951d";
        alert = "#${config.colorScheme.palette.base08}";
        black = "#585858";
        red = "#f2241f";
        green = "#67b11d";
        yellow = "#b1951d";
        blue = "#4f97d7";
        magenta = "#a31db1";
        cyan = "#2d9574";
        white = "#f8f8f8";
      };
      "bar/mybar" = {
        width = "100%";
        height = "20";
        radius = "0";
        fixed.center = "false";
        background = "#${config.colorScheme.palette.base00}";
        foreground = "#${config.colorScheme.palette.base04}";
        line.size = 2;
        line.color = "#f00";
        border.left.size = 3;
        border.right.size = 3;
        border.color = "#${config.colorScheme.palette.base00}";
        padding.left = 1;
        padding.right = 0;
        module.margin.left = 1;
        font = [
          "${nerdFont} Nerd Font:pixelsize=11;2"
        ];
        modules.left = "i3 xwindow";
        modules.center = "";
        modules.right = "filesystem xkeyboard pipewire-simple memory cpu date";
        separator = "  ";
        override.redirect = "false";
        scroll.up = "#i3.prev";
        scroll.down = "#i3.next";
        cursor.click = "pointer";
      };
      "module/xwindow" = {
        type = "internal/xwindow";
        label = "%title:0:30:...%";
      };
      "module/xkeyboard" = {
        type = "internal/xkeyboard";
        blacklist = ["num lock"];
        format-prefix = "  ";
      };
      "module/filesystem" = {
        type = "internal/fs";
        interval = 25;
        mount = ["/"];
        label.mounted = "󰆼 %percentage_used%%";
        label.unmounted = "%mountpoint% not mounted";
        label.unmounted-foreground = "#${config.colorScheme.palette.base05}";
      };
      "module/i3" = {
        type = "internal/i3";
        format = "<label-state> <label-mode>";
        format-prefix = "  ";
        index.sort = "true";
        wrapping.scroll = "false";
        label.mode.padding = 2;
        label.mode.foreground = "#000";
        label.mode.background = "#${config.colorScheme.palette.base02}";
        label.focused = "%index%";
        label.focused-background = "#${config.colorScheme.palette.base01}";
        label.focused-underline= "#${config.colorScheme.palette.base01}";
        label.focused-padding = 2;
        label.unfocused = "%index%";
        label.unfocused-padding = 2;
        label.visible = "%index%";
        label.visible-background = "#${config.colorScheme.palette.base01}";
        label.visible-underline = "#${config.colorScheme.palette.base01}";
        label.visible-padding = 2;
        label.urgent = "%index%";
        label.urgent-background = "#${config.colorScheme.palette.base08}";
        label.urgent-padding = 2;
      };
      "module/cpu" = {
        type = "internal/cpu";
        interval = 2;
        format.prefix = "󰻠 ";
        format.prefix-foreground = "#${config.colorScheme.palette.base05}";
        label = "%percentage:2%%";
      };
      "module/memory" = {
        type = "internal/memory";
        interval = 2;
        format.prefix = " ";
        format.prefix-foreground = "#${config.colorScheme.palette.base05}";
        label = "%percentage_used%%";
      };
      "module/date" = {
        type = "internal/date";
        interval = 5;
        date = "";
        date-alt = " %Y.%m.%d";
        time = "%H:%M ";
        time-alt = "%H:%M:%S ";
        format.prefix = " ";
        format.prefix-foreground = "#${config.colorScheme.palette.base05}";
        label = "%date% %time%";
      };
      "module/pipewire-simple" = {
        type = "custom/script";
        exec = pipewire_module_script;
        interval = 1;
        click.right = "exec ${pkgs.pavucontrol} &";
        click.left = "${pipewire_module_script} mute &";
        scroll.up = "${pipewire_module_script} up &";
        scroll.down = "${pipewire_module_script} down &";
      };
    };
  };
}
