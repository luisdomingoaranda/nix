{ config
, pkgs
, base16scheme
, nix-colors
, ...
} @ inputs:
{
  imports = [
    ./colorscheme.nix
  ];

  home.packages = with pkgs; [
    rofi # application launcher
    (nerdfonts.override { fonts = [ inputs.nerdFont ]; })
  ];

  services.sxhkd = {
    enable = true;
    extraConfig = ''
      # Rofi
      super + d
        rofi -show run
      alt + space
        rofi -show window
    '';
  };

  xdg.configFile = {
    "rofi/config.rasi".text = ''
      configuration {
        display-drun: "Applications:";
        display-window: "Windows:";
        font: "${inputs.nerdFont} Nerd Font";
        modi: "window,run,drun";
      }

      @theme "/dev/null"

      * {
        bg: #${config.colorScheme.palette.base00};
        bg-alt: #${config.colorScheme.palette.base01};

        fg: #${config.colorScheme.palette.base04};
        fg-alt: #${config.colorScheme.palette.base05};

        background-color: @bg;

        border: 0;
        margin: 0;
        padding: 0;
        spacing: 0;
      }

      window {
        width: 30%;
      }

      element {
        padding: 8 0;
        text-color: @fg-alt;
      }

      element selected {
        text-color: @fg;
      }

      element-text {
        background-color: inherit;
        text-color: inherit;
        vertical-align: 0.5;
      }

      element-icon {
        size: 30;
      }

      entry {
        background-color: @bg-alt;
        padding: 12;
        text-color: @fg;
      }

      inputbar {
        children: [prompt, entry];
      }

      listview {
        padding: 8 12;
        background-color: @bg;
        columns: 1;
        lines: 8;
      }

      mainbox {
        background-color: @bg;
        children: [inputbar, listview];
      }

      prompt {
        background-color: @bg-alt;
        enabled: true;
        padding: 12 0 0 12;
        text-color: @fg;
      }

      /* vim: ft=sass
    '';
  };

}
