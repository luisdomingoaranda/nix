{ pkgs
, nerdFont
, ...
} @ inputs:
{
  gtk = {
    enable = true;
    theme = {
      # name = "Graphite-dark";
      # package = pkgs.graphite-gtk-theme;
      name = "Tokyonight-Dark-BL";
      package = inputs.mynixpkgs.packages.${inputs.system}.tokyo-night-gtk-theme;
    };
    iconTheme = {
      # name = "Tela-blue-dark";
      # package = pkgs.tela-icon-theme;
      name = "Tokyonight-Light-Alt";
      package = inputs.mynixpkgs.packages.${inputs.system}.tokyo-night-gtk-theme;
    };
    cursorTheme = {
      # name = "Quintom_Ink";
      # package = pkgs.quintom-cursor-theme;
      name = "Simp1e-Tokyo-Night";
      package = inputs.mynixpkgs.packages.${inputs.system}.Simp1e-Tokyo-Night;
    };
    font = {
      name = "${nerdFont} Nerd Font";
      package = (pkgs.nerdfonts.override { fonts = [ nerdFont ]; });
      size = 11;
    };
  };

  # Required for qt.platformTheme = "gtk" to work
  xsession.enable = true;

  qt = {
    enable = true;
    platformTheme = "gtk";
    # platformTheme = "gnome";
    # style = {
    #   name = "adwaita-dark";
    #   package = pkgs.adwaita-qt;
    # };
  };
}
