{ config
, pkgs
, ...
} @ inputs:
{
  home.packages = with pkgs; [
    neovim

    tree-sitter
    # lua5_1
    luajit
    nodejs
    gcc

    # Formatters
    nixfmt-rfc-style # nix formatter
    shfmt # shell parser and formatter
    nodePackages.prettier # web langs formatter
    #php83Packages.php-codesniffer # php formatter

    # LSP's
    # nodePackages.vls # vue
    nodePackages.typescript-language-server # typescript / javascript
    clang-tools # clangd
    sumneko-lua-language-server # lua
    pyright
    #nodePackages.intelephense # php
    texlab

    # DAP's
    # e.g. python.withPackages (ps: with ps; [ debugpy ])
    lldb # C/C++/Rust
    vscode-extensions.ms-vscode.cpptools # C/C++/Rust

    # Other
    ripgrep # searcher (faster grep)
    fd # searcher
    imagemagick # convert bitmap images
  ];

  programs.git.extraConfig.core.editor = "nvim";

  xdg.configFile.nvim.source = pkgs.fetchFromGitLab {
    owner = "luisdomingoaranda";
    repo = "nvim";
    rev = "aea02c99421f4fedaaa7eb023ae0a3d773527e87";
    sha256 = "sha256-yDjHwsx4P+TDu5GOQb7MjizYXkYJqSuJcVEhyJ5udyE=";
  };

}
