{ config
, pkgs
, ...
} @ inputs:

let
  wallpaperPath = ../../assets/nix-wallpaper-nineish-dark-gray.png;
in

{
  imports = [
    ./waybar-hyprland/default.nix
    ./colorscheme.nix
  ];

  home.packages = with pkgs; [
    swww # wallpaper daemon
    fuzzel # app launcher
    xdg-desktop-portal-hyprland # Desktop integration portals for sandboxed apps
    dunst # notification daemon
    wl-clipboard # Command-line copy/paste utilities for Wayland
  ];

  # Launch hyprland automatically on login
  # programs = {
  #   zsh.loginExtra = ''
  #     if [ "$(tty)" = "/dev/tty1" ]; then
  #       exec Hyprland
  #     fi
  #   '';
  # };

  xdg.configFile."hypr/hyprland.conf".text = ''
    # See wiki for configuration options (https://wiki.hyprland.org/)

    # See https://wiki.hyprland.org/Configuring/Monitors/
    monitor=,preferred,auto,auto

    # See https://wiki.hyprland.org/Configuring/Keywords/ for more

    # Execute your favorite apps at launch
    exec-once = waybar &
    exec-once = dunst &
    exec-once = swww init & swww img ${wallpaperPath} &
    exec-once = chromium
    exec-once = nemo

    # Source a file (multi-file configs)
    # source = ~/.config/hypr/myColors.conf

    # Some default env vars.
    env = XCURSOR_SIZE, 24
    env = XDG_DATA_DIRS, $HOME/.nix-profile/share:/usr/local/share:/usr/share

    # For all categories, see https://wiki.hyprland.org/Configuring/Variables/
    input {
        kb_layout = us,es,ru
        kb_variant =
        kb_model =
        kb_options = grp:win_space_toggle,eurosign:e
        kb_rules =

        repeat_rate = 25
        repeat_delay = 200

        follow_mouse = 1

        touchpad {
            natural_scroll = false
        }

        sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
    }

    general {
        # See https://wiki.hyprland.org/Configuring/Variables/ for more

        gaps_in = 5
        gaps_out = 20
        border_size = 2
        col.active_border = rgba(33ccffee) rgba(00ff99ee) 45deg
        col.inactive_border = rgba(595959aa)

        layout = dwindle
    }

    decoration {
        # See https://wiki.hyprland.org/Configuring/Variables/ for more

        rounding = 10

        blur {
            enabled = true
            size = 3
            passes = 1
            # vibrancy = 0.1696
        }

        drop_shadow = true
        shadow_range = 4
        shadow_render_power = 3
        col.shadow = rgba(1a1a1aee)
    }

    animations {
        enabled = true

        # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

        bezier = myBezier, 0.05, 0.9, 0.1, 1.05

        animation = windows, 1, 7, myBezier
        animation = windowsOut, 1, 7, default, popin 80%
        animation = border, 1, 10, default
        animation = borderangle, 1, 8, default
        animation = fade, 1, 7, default
        animation = workspaces, 1, 6, default
    }

    dwindle {
        # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
        pseudotile = true # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
        preserve_split = true # you probably want this
    }

    master {
        # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
        new_is_master = true
    }

    gestures {
        # See https://wiki.hyprland.org/Configuring/Variables/ for more
        workspace_swipe = false
    }

    misc {
        # See https://wiki.hyprland.org/Configuring/Variables/ for more
        # force_default_wallpaper = 0 # Set to 0 to disable the anime mascot wallpapers
    }

    # Example per-device config
    # See https://wiki.hyprland.org/Configuring/Keywords/#per-device-input-configs for more
    device:epic-mouse-v1 {
        sensitivity = -0.5
    }

    # Example windowrule v1
    # windowrule = float, ^(kitty)$
    # Example windowrule v2
    # windowrulev2 = float,class:^(kitty)$,title:^(kitty)$
    # See https://wiki.hyprland.org/Configuring/Window-Rules/ for more
    windowrule = workspace 1, ^(Chromium-browser)$
    windowrule = workspace 2, ^(nemo)$


    # See https://wiki.hyprland.org/Configuring/Keywords/ for more
    $mainMod = SUPER
    $term = kitty

    # Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
    bind = $mainMod, return, exec, $term
    bind = $mainMod shift, q, killactive,
    bind = $mainMod, x, exit,
    bind = $mainMod, d, exec, fuzzel --background-color ${config.colorScheme.palette.base00}ff
    bind = $mainMod, p, pseudo, # dwindle
    bind = $mainMod, e, togglesplit, # dwindle
    bind = $mainMod, f, fullscreen
    bind = $mainMod shift, m, togglefloating

    # Stack windows
    bind = $mainMod, w, togglegroup
    bind = $mainMod alt, l, changegroupactive, f
    bind = $mainMod alt, h, changegroupactive, b
    bind = $mainMod SHIFT, h, movewindoworgroup, l
    bind = $mainMod SHIFT, l, movewindoworgroup, r
    bind = $mainMod SHIFT, k, movewindoworgroup, u
    bind = $mainMod SHIFT, j, movewindoworgroup, d


    # Move focus with mainMod + vim keys
    bind = $mainMod, h, movefocus, l
    bind = $mainMod, l, movefocus, r
    bind = $mainMod, k, movefocus, u
    bind = $mainMod, j, movefocus, d

    # Move windows with mainMod + Shift + vim keys
    bind = $mainMod shift, h, movewindow, l
    bind = $mainMod shift, l, movewindow, r
    bind = $mainMod shift, k, movewindow, u
    bind = $mainMod shift, j, movewindow, d

    # Resize
    bind = $mainMod control, h, resizeactive, -20 0
    bind = $mainMod control, l, resizeactive, 20 0
    bind = $mainMod control, k, resizeactive, 0 -20
    bind = $mainMod control, j, resizeactive, 0 20

    # Switch workspaces with mainMod + [0-9]
    bind = $mainMod, 1, workspace, 1
    bind = $mainMod, 2, workspace, 2
    bind = $mainMod, 3, workspace, 3
    bind = $mainMod, 4, workspace, 4
    bind = $mainMod, 5, workspace, 5
    bind = $mainMod, 6, workspace, 6
    bind = $mainMod, 7, workspace, 7
    bind = $mainMod, 8, workspace, 8
    bind = $mainMod, 9, workspace, 9
    bind = $mainMod, 0, workspace, 10

    # Move active window to a workspace with mainMod + shift + [0-9]
    bind = $mainMod shift, 1, movetoworkspace, 1
    bind = $mainMod shift, 2, movetoworkspace, 2
    bind = $mainMod shift, 3, movetoworkspace, 3
    bind = $mainMod shift, 4, movetoworkspace, 4
    bind = $mainMod shift, 5, movetoworkspace, 5
    bind = $mainMod shift, 6, movetoworkspace, 6
    bind = $mainMod shift, 7, movetoworkspace, 7
    bind = $mainMod shift, 8, movetoworkspace, 8
    bind = $mainMod shift, 9, movetoworkspace, 9
    bind = $mainMod shift, 0, movetoworkspace, 10

    # Example special workspace (scratchpad)
    bind = $mainMod, S, togglespecialworkspace, magic
    bind = $mainMod shift, S, movetoworkspace, special:magic

    # Scroll through existing workspaces with mainMod + scroll
    bind = $mainMod, mouse_down, workspace, e+1
    bind = $mainMod, mouse_up, workspace, e-1

    # Move/resize windows with mainMod + LMB/RMB and dragging
    bindm = $mainMod, mouse:272, movewindow
    bindm = $mainMod, mouse:273, resizewindow
  '';

}
