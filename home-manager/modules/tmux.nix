{ config
, pkgs
, ...
} @ inputs:

{
  programs.tmux = {
    enable = true;
    extraConfig = ''
      # Avoid delay when pressing <ESC>
      set -s escape-time 0

      # window names on top-center
      set -g status-justify centre
      set -g status-position top

      # vim-like pane resizing
      bind -r C-k resize-pane -U
      bind -r C-j resize-pane -D
      bind -r C-h resize-pane -L
      bind -r C-l resize-pane -R

      # vim-like pane switching
      bind -r k select-pane -U
      bind -r j select-pane -D
      bind -r h select-pane -L
      bind -r l select-pane -R

      # tmux display things in 256 colors
      set -g default-terminal "tmux-256color"
      set -ag terminal-overrides ",xterm-256color:RGB"

      # render images
      set -gq allow-passthrough on
      set -g visual-activity off

      # Transparent bottom bar
      set -g status-style bg=default

      # Set vim keybindings for copy mode
      # Enter copy mode: `Ctrl+b,[`
      # Exit copy mode: `q`
      set-window-option -g mode-keys vi
      bind-key -T copy-mode-vi v send-keys -X begin-selection
      bind-key -T copy-mode-vi V send-keys -X select-line
      bind-key -T copy-mode-vi y send-keys -X copy-pipe 'xclip -in -selection clipboard'
    '';
    plugins = [
      {
        plugin = pkgs.tmuxPlugins.resurrect;
        extraConfig = ''
          # By default tmux-resurrect only restarts some processes
          # e.g. nvim, top, less, cat, etc
          set -g @resurrect-processes '\
            cmatrix
            top
            man
            ssh
            "sudo docker-compose up"
          '
        '';
      }
    ];
  };
}
