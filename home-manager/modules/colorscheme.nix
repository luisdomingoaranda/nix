{ base16scheme
, nix-colors
, ...
} @ inputs:
{
  # Module to be imported by any other module that uses the base16 colorscheme
  # Base16 colorscheme options: https://github.com/tinted-theming/base16-schemes
  # How to style: https://github.com/chriskempson/base16/blob/main/styling.md

  imports = [
    nix-colors.homeManagerModule
  ];

  colorscheme = nix-colors.colorSchemes.${base16scheme};
}
