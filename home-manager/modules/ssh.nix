{ config
, pkgs
, ...
} @ inputs:
{
  # SSH client

  home.packages = with pkgs; [
    openssh # An implementation of the SSH protocol
    sshpass # Pass user and password to ssh command
    sshfs # Mount remote FS over ssh
  ];

  programs.ssh = {
    enable = true;
    forwardAgent = true;
    extraConfig = ''
      Host nas
          Hostname luisdomingoaranda.com
          User luis
          # RemoteForward <remote_agent_socket> <local_agent_socket>
          RemoteForward /run/user/1001/gnupg/S.gpg-agent /run/user/1000/gnupg/S.gpg-agent

      Host nas_local
          Hostname 192.168.1.148
          User luis
          # RemoteForward <remote_agent_socket> <local_agent_socket>
          RemoteForward /run/user/1001/gnupg/S.gpg-agent /run/user/1000/gnupg/S.gpg-agent
    '';
  };
}
