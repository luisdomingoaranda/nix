{ config
, pkgs
, ...
} @ inputs:
{
  imports = [
    ./polybar.nix
    ./rofi.nix
  ];


  # File to execute from i3 config
  # Since .xprofile won't work without a display manager (i.e. lightdm)
  # And launching some daemons on .xinitrc before i3 gives problems (e.g. sxhkd)
  home.file.".i3-autostart.sh".text =
    let
      wallpaperPath = ../../assets/nix-wallpaper-nineish-dark-gray.png;
    in
    ''
      # Set screen resolution
      # If x preferred resolution is less than 1920, then set 1920 resolution
      x_pref=$(xrandr -q | grep -Pi "\+" | grep -Pio "^\s+\K(\d+)(?=x)")
      if [ $x_pref -le 1920 ]; then
        xrandr -s 1920x1080
      fi

      # Set wallpaper
      ${pkgs.feh}/bin/feh --bg-scale ${wallpaperPath}

      # Disable screensaver and autosuspend
      # Params:
      # s off (screensaver off)
      # -dpms (disables energy star features)
      ${pkgs.xorg.xset}/bin/xset s off -dpms

      # Set keyboard repeat speed
      ${pkgs.xorg.xset}/bin/xset r rate 300 50

      # Start daemons
      sxhkd &

      # Set keyboard languages
      # Requires delay (sleep) because of issue with initialisation time for keyboard at startup
      # see: https://www.linuxquestions.org/questions/linux-software-2/%5Bsolved%5D-setxkbmap-to-change-the-keyboard-layout-doesn%27t-work-during-fast-boots-4175723061/
      sleep 1
      ${pkgs.xorg.setxkbmap}/bin/setxkbmap -layout us,es,ru -option grp:win_space_toggle
    '';

  xsession = {
    enable = true;
    windowManager.i3.enable = true;
  };


  xdg.configFile = {
    i3.source = pkgs.fetchFromGitLab {
      owner = "luisdomingoaranda";
      repo = "i3";
      rev = "712b31e25a28b59ae56f03b8f500ec59f7de8023";
      hash = "sha256-/9Ti0Uo/Qxh/XdeKc0viFcjv4wCSuRVSUEDJJKUxj74=";
    };

    # polybar.source = pkgs.fetchFromGitLab {
    #   owner = "luisdomingoaranda";
    #   repo = "polybar";
    #   rev = "85a75df9b79925f59215581b9bdff9b82c66b2ab";
    #   sha256 = "sha256-gQX0ziHO1gLpxhi7wpxdYbaCvYc+kuLasZaToEvGmkQ=";
    # };
  };

  # Keybindings
  services.sxhkd = {
    enable = true;
    extraConfig = ''
      # Basics
      super + Return
        $TERMINAL

      # Scrot screenshots
      Print
        scrot -m $HOME/Pictures/%s_%H%M_%d.%m.%Y_$wx$h.png
      shift + @Print
        scrot -s $HOME/Pictures/%s_%H%M_%d%m%Y_$wx$h.png
      ctrl + shift + @Print
        scrot -s $HOME/Pictures/screenshot_tmp.png && xclip -t image/png -selection clipboard $HOME/Pictures/screenshot_tmp.png && rm $HOME/Pictures/screenshot_tmp.png

      # Volume controls
      control + shift + Up
        pamixer --increase 1
      control + shift + Down
        pamixer --decrease 1
    '';
  };

  # GTK styles
  home.file.".icons".source = ../../assets/.icons; # icons & cursors
  home.file.".themes".source = ../../assets/.themes; # themes

  # Append to .Xresources
  xresources.extraConfig = ''
    # Any folder name from .icons
    Xcursor.theme: Simp1e-Tokyo-Night
    Xft.dpi: 96
  '';

  home.packages = with pkgs; [
    dmenu # application launcher
    feh # image viewer and set background img
    xorg.xev # identify keycodes in xorg
    # (polybar.override { i3Support = true; }) # status bar
    i3status # status bar
    scrot # take screenshots
    xclip # clipboard
    xorg.xset # set user preferences for X
    xorg.setxkbmap # set keyboard layouts
    xorg.xcursorthemes # cursor themes
    i3lock # lock window
    pamixer # audio
    pavucontrol # audio
  ];

  # Xorg compositor for transparency windows, etc. (with i3wm)
  services.picom = {
    enable = true;
    opacityRules = [
      "90:class_g = 'URxvt' && focused"
      "60:class_g = 'URxvt' && !focused"
      "90:class_g = 'kitty' && focused"
      "60:class_g = 'kitty' && !focused"
    ];
  };

}
