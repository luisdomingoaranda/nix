{ config
, pkgs
, ...
} @ inputs:

{
  home.packages = with pkgs; [
    texlive.combined.scheme-full # latex distribution with all packages
    texstudio
  ];
}
