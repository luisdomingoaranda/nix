{ self
, home-manager-unstable
, ...
} @ inputs:

let
  pkgsForSystem = system: import inputs.nixpkgs-unstable {
    config.allowUnfree = true;
    inherit system;
    overlays = [ inputs.nixgl.overlay ];
  };
  # Nerd font options: https://github.com/ryanoasis/nerd-fonts
  nerdFont = "FiraCode";
  # Base16 colorscheme options: https://github.com/tinted-theming/base16-schemes
  # How to style: https://github.com/chriskempson/base16/blob/main/styling.md
  base16scheme = "tokyo-night-terminal-dark";
in
{

  arch =
    let
      username = "luis";
      system = "x86_64-linux";
      specialArgs = inputs // { inherit username system nerdFont base16scheme; };
      pkgs = pkgsForSystem system;
    in
    home-manager-unstable.lib.homeManagerConfiguration {
      inherit pkgs;
      extraSpecialArgs = specialArgs;
      modules = [
        ./hosts/arch.nix
      ];
    };

  wsl =
    let
      username = "luis";
      system = "x86_64-linux";
      specialArgs = inputs // { inherit username system base16scheme; };
      pkgs = pkgsForSystem system;
    in
    home-manager-unstable.lib.homeManagerConfiguration {
      inherit pkgs;
      extraSpecialArgs = specialArgs;
      modules = [
        ./hosts/wsl.nix
      ];
    };

}
