{
  EDITOR = "nvim";

  # Machines
  CI_DESA = "ci_desa";
  CI_PROD = "ci_prod";
  IRIS_DESA = "iris_desa";
  IRIS_PROD = "iris_prod";
  AIX = "aix";
}
