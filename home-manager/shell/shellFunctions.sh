# Shortcut to open your workspaces
f()
{
    option=$(echo "\
      dirs\n\
      music\n\
      videos\n\
      emojis\n\
      scripts\n\
    " | sed 's/^\s\+//' | fzf)

    case $option in
      dirs)
        # List of directories
        dirs="
          $(ls -d /home/luis/dev/*/)
          $(ls -d /home/luis/dev/embedded/*/)
          $(ls -d /home/luis/dev/web/*/)
          $(ls -d /home/luis/dev/repls/*/)
          $(ls -d /home/luis/Documents/*/)
          $(ls -d /home/luis/Documents/Library/*/)
          $(ls -d /home/luis/dev/entelgy/*/)
          $(ls -d /home/luis/dev/entelgy/sigpe-sao/*/)
          $(ls -d /home/luis/dev/entelgy/sigpe-sao/sigpe-multientidad/src/main/*/)
          $(ls -d /home/luis/dev/entelgy/accure/*/)
          $(ls -d /home/luis/dev/entelgy/accure/accure/src/main/*/)
          $(ls -d /home/luis/dev/entelgy/rto/*/)
          $(ls -d /home/luis/dev/entelgy/rto/rto-premium/src/main/*/)
          $(ls -d /home/luis/dev/entelgy/sac/*/)
          $(ls -d /home/luis/dev/entelgy/sac/sac-back/src/main/*/)
          $(ls -d /home/luis/dev/entelgy/navarra/ventanilla/*/)
          $(ls -d /home/luis/dev/entelgy/navarra/comite/*/)
          $(ls -d /home/luis/dev/entelgy/navarra/intranet_navarra/*/)
          $(ls -d /home/luis/dev/entelgy/intranet-no-grupo/intranet-no-grupo/src/main/*/)
          $(ls -d /home/luis/dev/entelgy/sigpe-bce/sigpe-bce/src/main/*/)
          $(ls -d /home/luis/dev/entelgy/intranet_grupo/nuevaintranetgrupo/src/main/*/)
        "
        # Remove leading spaces
        dirs=$(echo $dirs | sed 's/^\s\+//')

        # Select directory using fzf
        cd $(echo $dirs | fzf)
        nix-shell 2>/dev/null
        ;;

      videos)
        video=$(find ~/Videos | fzf)
        echo $video
        if [ $video ]; then
          echo $video
          vlc "$video"
        fi
        ;;

      music)
        songs=$(find ~/Music | fzf --multi)
        songs=$(echo $songs | sed 's/^/ "/' | tr '\n' '"')
        if [ $songs ]; then
          echo $songs | xargs vlc --random --loop
        fi
        ;;

      emojis)
        emojis_txt_path=/home/luis/Documents/nix/mynixsystems/home-manager/shell/files/emojis.txt
        emoji=$(                  \
            cat $emojis_txt_path  \
          | fzf                   \
          | grep -Po "^."         \
        )
        # copy emoji character to clipboard
        echo $emoji | xclip -selection clipboard
        ;;

      scripts)
        scripts="
          android_mount
          android_unmount
          mount_vm_shared_folder
          load_env_dirs
          mp4togif
          set_screen
          vpn_rsi
          vpn_upm
          record_mp3
        "
        # Remove leading spaces
        scripts=$(echo $scripts | sed 's/^\s\+//')

        # Select script using fzf
        $(echo $scripts | fzf)
        ;;

    esac

}


android_mount()
{
    sudo jmtpfs /home/$USER/android_mount_dir -o allow_other;
    mkdir /home/$USER/android_mount_dir;
    echo "Montado en /home/$USER/android_mount_dir";
}


android_unmount()
{
    sudo fusermount -u /home/$USER/android_mount_dir;
    rmdir /home/$USER/android_mount_dir;
}


mount_vm_shared_folder()
{
    sudo vmhgfs-fuse .host:/vm_nixos_shared /mnt/hgfs
}


mp4togif()
{
    # param $1 input mp4 filename
    input_filename=$1
    # param $2 output gif filename
    output_filename=$2

    if [ -z "$2" ]; then
        output_filename=$1
    fi

    ffmpeg -i ${input_filename}.mp4 ${output_filename}.gif
}


# Setup screen resolution with xrandr to 1920x1080
set_screen()
{
    # XRES=$1
    # YRES=$2
    # MODELINE=$(gtf ${XRES} ${YRES} 60 | grep Modeline | cut -c 12- | awk 'NF{NF-=2};1')
    # MODE=$(echo ${MODELINE} | awk '{print $1}' | tr -d '"')
    # xrandr --newmode $(echo ${MODELINE})
    # xrandr --addmode Virtual1 ${MODE}
    # xrandr --output Virtual1 --mode ${MODE}

    xrandr --newmode "1920x1080_60.00" 172.80 1920 2040 2248 2576 1080 1081 1084 1118
    xrandr --addmode Virtual-1 1920x1080_60.00
    xrandr --output Virtual-1 --mode 1920x1080_60.00
    kill -9 $(ps aux | grep bin/polybar | head -1 | awk '{print $2}')
}


vpn_rsi()
{
    sudo openconnect --protocol=anyconnect --user=u028693 vpn.ruralserviciosinformaticos.com/RSI_OTP;
}


vpn_upm()
{
    sudo openfortivpn vpnssl.upm.es --username=ld.aranda@alumnos.upm.es;
}


record_mp3()
{
    ffmpeg \
      -f pulse \
      -i alsa_output.pci-0000_00_1b.0.analog-stereo.monitor \
      -acodec libmp3lame \
      output.mp3
}


#####
# RSI
#####
up()
{
    # Go up to the machine root folder
    MACHINEROOTS="$CI_DESA $CI_PROD $IRIS_DESA $IRIS_PROD"
    dirName=$(basename "$PWD")

    if echo $MACHINEROOTS | grep -w $dirName >/dev/null; then
        return
    else
        cd ..
        up
    fi
}


load_env_dirs()
{
    MACHINE=$1

    # Load env dir shortcuts
    case $MACHINE in
        $CI_DESA)
            export home=$(echo $(up && cd home/u028693/ && pwd))
            export docum=$(echo $(up && rsicif/sysdes/desxld/docum2/ && pwd))
            alias home="cd $(echo $(up && cd home/u028693/ && pwd))"
            alias syspwi="up && cd rsicif/syspwi"
            alias syspri="up && cd lnxbcip1/rsicif/syspri"
            alias lrecl="up && cd usr/local/bphx/data/lrecl_dat_dir"
            alias entelgy="up && cd rsicif/sysdes/entelgy/"
            alias proc="up && cd rsicif/sysdes/desxld/proclib/"
            alias docum="up && cd rsicif/sysdes/desxld/docum2/"
            alias contora="up && cd rsicif/sysdes/desxld/contora/"
            alias control="up && cd rsicif/sysdes/desxld/control/"
            alias unloadcopys="up && cd rsicif/sysdes/desxld/unloadcopys/"
            alias a_proc="up && cd lnxbcip1/rsicif/sysexi/rsi/cifaprod/proclib/"
            alias a_contora="up && cd lnxbcip1/rsicif/sysexi/rsi/cifaprod/contora/"
            alias a_control="up && cd lnxbcip1/rsicif/sysexi/rsi/cifaprod/control/"
            alias a_unloadcopys="up && cd lnxbcip1/rsicif/sysexi/rsi/cifaprod/unloadcopys/"
            ;;
        $CI_PROD) ;;

        $IRIS_DESA)
            export mhome=$(echo $(up && cd home/u028693/ && pwd))
            alias home="cd $(echo $(up && cd home/u028693/ && pwd))"
            alias proc="up && rsiiri/sysdes/desxld/cons/proclib"
            alias docum="up && rsiiri/sysdes/desxld/cons/docum"
            alias a_proc="up && zlpro01/rsiiri/sysexi/rsi/iriaprod/proclib"
            ;;
        $IRIS_PROD) ;;

        *) ;;

    esac
    return
}
