2prod()
{
    # Script para convertir cadenas que se han estado usando para probar en
    # desarrollo, a cadenas que se puedan subir al entorno de produccion.
    # Basicamente hace
    # - Borra la linea de "export TWO_TASK="
    # - Cambia las rutas de los elementos de desarrollo (contoras, controls, etc),
    #   de desarrollo a produccion.
    # - Quita los valores que puedas tener asignados a los parametros de la linea
    #   handle_parms.
    #
    # Entrada:
    # - $1: cadena a modificar
    # - $2: archivo donde escribir la salida (cadena modificada)

    cadena_orig=$1
    nombre_cadena_orig=$(echo $1 | sed 's,^.*\/,,' | cut -c 1-8)
    cadena_dest=$2
    nombre_cadena_dest=$(echo $2 | sed 's,^.*\/,,' | cut -c 1-8)

    echo "--------------------------------------------------------------"
    echo "Pasa las cadenas de pruebas al formato de subida a produccion"
    echo "--------------------------------------------------------------"

    if [ $HOST = "lnxdci01" ]; then
        echo "Maquina LNXDCI01"
        echo "----------------"
        ruta_desarrollo=/rsicif/sysdes/desxld
        ruta_produccion=/rsicif/sysexi/rsi/cifaprod
    elif [ $HOST = "zlfor01" ]; then
        echo "Maquina ZLFOR01"
        echo "---------------"
        ruta_desarrollo=/rsiiri/sysdes/desxld/cons
        ruta_produccion=/rsiiri/sysexi/rsi/iriaprod
    else
        echo "ERROR: La maquina actual no es ni lnxdci01 ni la zlfor01"
        exit 1 # terminate and indicate error
    fi

    # Escapa las barras / por \/ en las rutas
    ruta_desarrollo_escaped=$(echo $ruta_desarrollo | sed 's,/,\\/,g')
    ruta_produccion_escaped=$(echo $ruta_produccion | sed 's,/,\\/,g')
    prefijo_zlpro01=/zlpro01
    prefijo_lnxbcip1=/lnxbcip1
    prefijo_lnxbcip1_escaped=$(echo $prefijo_lnxbcip1 | sed 's,/,\\/,g')
    prefijo_zlpro01_escaped=$(echo $prefijo_zlpro01 | sed 's,/,\\/,g')

    # Rutas de ficheros
    rsicif_formacion=/rsicif/sysprb
    rsicif_formacion_escaped=$(echo $rsicif_formacion | sed 's,/,\\/,g')
    rsicif_produccion=/rsicif/syspri
    rsicif_produccion_escaped=$(echo $rsicif_produccion | sed 's,/,\\/,g')
    rsiiri_formacion=/rsicif/sysprb
    rsiiri_formacion_escaped=$(echo $rsiiri_formacion | sed 's,/,\\/,g')
    rsiiri_produccion=/rsicif/syspri
    rsirii_produccion_escaped=$(echo $rsiiri_produccion | sed 's,/,\\/,g')

    # Nueva linea con los argumentos vacios
    parameters=$(grep -Pio "RSI_PA_\w+" $cadena_orig | cut -c 8- | sort -u)
    handle_params=". handle_params.ksh $nombre_cadena_dest \"\$@\""
    for parameter in ${parameters[@]}; do
        handle_params="$handle_params"" -$parameter ''"
    done

    # 1. Borra la linea de export TWO_TASK='CID' en caso de existir
    # 2. Borra prefijos /lnxbcip1
    # 3. Borra prefijos /zlpro01
    # 4. Sustituye la linea del handle_params
    # 5. Sustituye rutas de desarrollo a produccion para los elementos
    # 6. Descomenta las lineas de la docum
    # 7. Sustituye el nombre de la cadena de entrada por el nombre de salida.
    # 8. Sustituye rutas de ficheros rsicif de formacion a produccion
    # 9. Sustituye rutas de ficheros rsiiri de formacion a produccion
    sed -e '/^\s*export\s*TWO_TASK/d' ${cadena_orig} |
        sed -e "s/$prefijo_zlpro01_escaped//g" |
        sed -e "s/$prefijo_lnxbcip1_escaped//g" |
        sed -e "s/.*handle_params.*/$handle_params/" |
        sed -e "s/${ruta_desarrollo_escaped}/${ruta_produccion_escaped}/g" |
        sed -e 's,^#\/\/\*,\/\/\*,g' |
        sed -e "s,${nombre_cadena_orig},${nombre_cadena_dest},g" |
        sed -e "s/${rsicif_formacion_escaped}/${rsicif_produccion_escaped}/g" |
        sed -e "s/${rsiiri_formacion_escaped}/${rsiiri_produccion_escaped}/g" \
            >tmp_2prod.ksh

    mv tmp_2prod.ksh $cadena_dest

    echo "          "
    echo "COMPLETADO"
}
