eval_cadena()
{
    # Importa las funciones
    #. ~/scripts/pasos.sh
    source ~/.config/nixpkgs/shell/rsi/pasos.sh

    # Variables
    # El nombre de la cadena son los 8 primeros caracteres del archivo de entrada
    nombre_cadena_entrada=$(echo $1 | sed 's,^.*\/,,' | cut -c 1-8)
    nombre_cadena=$(echo $2 | sed 's,^.*\/,,' | cut -c 1-8)
    archivo_temporal="tmp_eval_cadena.ksh"

    # Evalua los comandos escritos en la cadena
    # Exact lines, no trimming
    while IFS= read -r line; do
        # Si la linea empieza con el simbolo, evalua el comando
        first_char=$(echo $line | cut -c 1)
        if [[ $first_char == @ ]]; then

            command=$(echo $line | cut -c 2-)
            eval "${command}"

            # Si no, copia la linea
        else
            printf '%s\n' "$line"
        fi
    done <$1 >$archivo_temporal

    # Renombra las funciones con el nombre de la cadena
    sed -i "s/${nombre_cadena_entrada}/${nombre_cadena}/g" $archivo_temporal
    sed "s/XXXXXXXX/$nombre_cadena/g" $archivo_temporal
}

cad()
{
    # El nombre de la cadena son los 8 primeros caracteres del archivo de entrada
    fichero_entrada=$1
    fichero_salida=$2
    nombre_cadena_entrada=$(echo $1 | sed 's,^.*\/,,' | cut -c 1-8)
    nombre_cadena=$(echo $2 | sed 's,^.*\/,,' | cut -c 1-8)

    # Evalua la cadena
    eval_cadena $fichero_entrada $nombre_cadena >tmp.ksh

    echo "Evaluacion de funciones..."
    ###########
    # Variables
    ###########
    # Lista de todos los pasos encontrados (e.g. [P010 ABENP010 FIBEP010 P020 ...])
    pasos_array=($(grep -Pi "process_[PAF].*\(\)" tmp.ksh | sed 's/^\s*//' | cut -c 18- | sed 's/()//'))

    n_pasos_array=$(echo "${pasos_array[@]}" | tr -s ' ' '\n' | grep -Po "PR?\d+" | uniq)

    # handle parametros
    parametros=$(grep -Pio "RSI_PA_\w+" tmp.ksh | cut -c 8- | sort -u)

    #######################
    # Construye secciones #
    #######################
    ##################################
    # Construye parte documentacion
    ##################################
    echo "documentacion..."

    ji_old=$(grep -Pi "\*JI-" tmp.ksh)
    if [ -z $ji_old ]; then
        ji="#//*JI-${nombre_cadena}-"
    else
        ji="$ji_old"
    fi

    pd=""
    for parametro in $(echo $parametros); do
        pd_line=$(grep -Pi "\*PD-.*${parametro}\b" tmp.ksh)
        ruta_produccion_docum=$docum
        if [ -z $pd_line ]; then
            if [ -d $ruta_produccion_docum ]; then
                param_doc=$(grep -i "${parametro}-" ${ruta_produccion_docum}/* | head -1 | sed 's,^.*:,,')
            else
                param_doc="//*PD-${parametro}-"
            fi
            pd="$pd""
                #$param_doc"
        else
            pd="$pd""
                ${pd_line}"
        fi
    done

    sd=""
    for n_paso in $(echo $n_pasos_array); do
        sd_line=$(grep -Pi "\*SD-.*\b${n_paso}\b" tmp.ksh)
        if [ -z $sd_line ]; then

            primera_funcion_paso=$(echo ${pasos_array[@]} |
                tr -s ' ' '\n' |
                grep -Pi $n_paso |
                head -1)

            sd_description=$(grep -B 5 "........_process_${primera_funcion_paso}()" tmp.ksh |
                sed -n '/#.[#-][#-]/,/#.[#-][#-]/p' |
                sed '/^\s*#.[#-][#-]/d' |
                sed 's/^\s*#\s*//')

            sd="$sd""
                #//*SD-${n_paso}-${sd_description}"
        else
            sd="$sd""
                ${sd_line}"
        fi
    done

    sr=""
    for n_paso in $(echo $n_pasos_array); do
        sr_line=$(grep -Pi "\*SR-.*${n_paso}\b" tmp.ksh)
        if [ -z $sr_line ]; then
            sr="$sr""
                #//*SR-${n_paso}-${n_paso}"
        else
            sr="$sr""
                ${sr_line}"
        fi
    done

    jp_old=$(grep -Pi "\*JP-" tmp.ksh)
    if [ -z $jp_old ]; then
        jp=""
    else
        jp="
        $jp_old"
    fi

    cm_old=$(grep -Pi "\*CM-" tmp.ksh)
    if [ -z $cm_old ]; then
        cm="#//*CM-"
    else
        cm=$cm_old
    fi

    pr_old=$(grep -Pi "\*PR-" tmp.ksh)
    if [ -z $pr_old ]; then
        pr="#//*PR-P3"
    else
        pr=$pr_old
    fi

    documentacion="$ji $pd $sd $sr $jp
$cm
$pr"

    ##################################

    ##################################
    # Construye pasos
    ##################################
    echo "pasos..."

    pasos_old=$(sed -n '/usr\/bin/,/process_step()/p' tmp.ksh |
        head -n -1 |
        tail -n +2)
    # Si la cadena no tiene usr/bin o process_step, asumo que
    # solo contiene informacion de pasos
    if [ -z $pasos_old ]; then
        pasos=$(cat tmp.ksh)
    else
        pasos=$pasos_old
    fi
    ##################################

    ##################################
    # Construye trazas
    ##################################
    echo "Trazas..."

    # Si las trazas no van contenidas en la seccion con los pasos,
    # pon una plantilla para trazas.
    has_trazas=$(echo $pasos | grep -Pi "(USUARIO|AUTOR)")
    trazas=""
    if [ -z $has_trazas ]; then
        trazas="
        # CREACION :
        #-----------
        #
        # REFERENCIA FECHA      AUTOR   DESCRIPCION
        # ---------- ---------- ------- ------------------------------------------------------------
        #
        #-------------------------------------------------------------------------------------------
        # MODIFICACIONES :
        #-----------------
        #
        # REFERENCIA FECHA      AUTOR   DESCRIPCION
        # ---------- ---------- ------- ------------------------------------------------------------
        #-------------------------------------------------------------------------------------------"
    else
        trazas=""
        # El resto de la traza ya va incluida en la cabecera de la variable $pasos
    fi
    ##################################

    ##################################
    # Construye process_step
    ##################################
    echo "process_step..."

    process_step="
${nombre_cadena}_process_step()
{
export RSI_SUBPHASE=\"\$2\"
if . enter_this_step.ksh \$1
then
. start_step.ksh \$1
case \$1 in
"
    for process in ${pasos_array[@]}; do
        process_step="$process_step""
        \"$process\")
        ${nombre_cadena}_process_${process}
        ;; "
    done

    process_step="$process_step""
*)
echo \"Fatal Error in ${nombre_cadena}: Unknown step of \$1\"
do_exit
;;
esac
. end_step.ksh \$1
fi
}"
    ##################################

    ##################################
    # Construye global_process
    ##################################
    echo "global_process..."

    global_process="
${nombre_cadena}_global_process()
{"

    for process in ${pasos_array[@]}; do
        global_process="$global_process""
        ${nombre_cadena}_process_step $process \$RSI_SUBPHASE"
    done

    global_process="$global_process""
}"
    ##################################

    ##################################
    # Construye two_process_step
    ##################################
    echo "two_process_step..."

    two_process_step="
${nombre_cadena}_two_process_step()
{
  ${nombre_cadena}_process_step \$1 \"PRERUN\"
  ${nombre_cadena}_process_step \$1 \"POSTRUN\"
}"
    ##################################

    ##################################
    # Construye comentario_run
    ##################################
    pasos_run_process=($(sed -n '/run_process()/,/}/p' tmp.ksh |
        grep -Pi "two_process_step " |
        grep -Po "[PAF].*"))

    # Pasos que no estan en el run_process se añaden al inicio del run_process, pero comentados
    dif_pasos=($(echo ${pasos_array[@]} ${pasos_run_process[@]} ${pasos_run_process[@]} |
        tr -s ' ' '\n' |
        sort |
        uniq -u))

    pasos_comentario=""
    for paso in ${pasos_array[@]}; do
        pasos_comentario=$pasos_comentario"
        $(echo ${dif_pasos[@]} | tr -s ' ' '\n' | grep -Pi "\b$paso\b")"
    done

    n_dif_pasos=($(echo ${pasos_comentario[@]} |
        tr -s ' ' '\n' |
        grep -Po "\d+" |
        awk '!a[$0]++'))

    # Ordena los dif_pasos como: PXXX; ABENPXXX; FIBEPXXX
    dif_pasos_sorted=""

    for n_dif_paso in ${n_dif_pasos[@]}; do

        PXXX=$(echo ${dif_pasos[@]} | tr -s ' ' '\n' | grep -Pi "PR?${n_dif_paso}\b" | grep -Pi "^P")
        ABENPXXX=$(echo ${dif_pasos[@]} | tr -s ' ' '\n' | grep -Pi "ABENPR?${n_dif_paso}\b" | grep -Pi "^A")
        FIBEPXXX=$(echo ${dif_pasos[@]} | tr -s ' ' '\n' | grep -Pi "FIBEPR?${n_dif_paso}\b" | grep -Pi "^F")

        dif_pasos_sorted="$dif_pasos_sorted""
        $PXXX
        $ABENPXXX
        $FIBEPXXX
        "
    done

    comentario_run=""

    for paso in $(echo $dif_pasos_sorted); do
        if [ $(echo $paso | grep "ABEN") ]; then

            comentario_run="$comentario_run""
                #if ( . tester.ksh $(echo $paso | grep -Po "P\d+") -ne 0 )
                #then
                #  ${nombre_cadena}_two_process_step $paso
                #fi"

        elif [ $(echo $paso | grep "FIBE") ]; then
            comentario_run="#$comentario_run""
                #  ${nombre_cadena}_two_process_step $paso
                "
        else
            comentario_run="$comentario_run""
                #${nombre_cadena}_two_process_step $paso"
        fi
    done
    ##################################

    ##################################
    # Construye run_process
    ##################################
    echo "run_process..."

    # run_process
    run_process_old=$(sed -n '/run_process()/,/}/p' tmp.ksh)
    if [ -z $run_process_old ]; then
        run_process="
        ${nombre_cadena}_run_process()
        {
        }
        "
    else
        run_process=$run_process_old
    fi
    ##################################

    ##################################
    # Pie de la cadena
    ##################################
    echo "pie de cadena..."

    # Rellena los parametros desde zero
    handle_params=". handle_params.ksh $nombre_cadena \"\$@\""
    for parameter in $(echo $parametros); do
        handle_params="$handle_params"" -$parameter ''"
    done

    main="
. start_proc.ksh $nombre_cadena

if [ \"\$RSI_PHASE\" = \"RUN\" ]
then
if [ \"\$RSI_SUBPHASE\" = \"PRERUN\" ]
then
${nombre_cadena}_run_process
export RSI_SUBPHASE=\"PRERUN\"
fi
else
        ${nombre_cadena}_global_process
fi
. end_proc.ksh $nombre_cadena"

    pie="$handle_params
$main"

    ###################################################
    # Concatena todas las secciones en el archivo final
    ###################################################
    cadena="$documentacion
#!/usr/bin/ksh $trazas
$pasos

$process_step

$global_process

$two_process_step

$comentario_run
$run_process

$pie"

    echo "$cadena" >tmp1.ksh

    ##################
    # Retoques finales
    ##################
    # Identa la cadena usando vim
    # " archivos sin extension se colorearan como shell scripts por defecto
    # autocmd BufNewFile,BufRead * if expand('%:t') !~ '\.' | set syntax=sh | set filetype=sh | endif
    # filetype indent on
    echo "Identa cadena..."

    nvim -c "Neoformat" -c "wq" tmp1.ksh

    # Borra todos los archivos temporales
    mv tmp1.ksh $fichero_salida
    rm ./tmp*

    echo "Completado"
}
