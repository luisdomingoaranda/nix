# Corta y aplantilla un fichero con la longitud que le pongas
# Param $1: fichero
# Param $2: longitud
# Param $3 (opcional): numero de caracteres a procesar
# e.g. "aplantilla g0214.p03 95"         // no corta el fichero
# e.g. "aplantilla g0214.p03 95 100000"  // solo coge las 100000 primeras posiciones del fichero
aplantilla()
{
    cut -c 1-$3 <$1 | sed 's/[^[:print:]]/ /g' | fold -w $2 | nvim -
}
