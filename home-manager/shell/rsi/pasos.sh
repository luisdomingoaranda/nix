# SQLPLUS
# Argumentos:
# -p, --paso: paso
# -d, --descripcion: descripcion
# -c, --contora: contora a ejecutar
# Al final, una lista de uno o mas argumentos que se pasan a la contora
#
# Ejemplos:
# @ sql --paso '020' --descripcion 'SQLPLUS-EJECUCION DE PL-SQL' --contora /rsicif/sysdes/desxld/contora/mi_contora.sql '${RSI_PA_FEC8P}' '${RSI_PA_DATAMNY1}'
#
sql()
{

    POSITIONAL=""
    while [[ $# -gt 0 ]]; do
        key="$1"
        case $key in
            -p | --paso)
                PASO="$2"
                shift # past argument
                shift # past value
                ;;
            -d | --descripcion)
                DESCRIPCION="$2"
                shift # past argument
                shift # past value
                ;;
            -c | --contora)
                CONTORA="$2"
                shift # past argument
                shift # past value
                ;;
            --args)
                DEFAULT=YES
                shift # past argument
                ;;
            *)                              # unknown option
                POSITIONAL="$POSITIONAL $1" # save it in an array for later
                shift                       # past argument
                ;;
        esac
    done
    set -- "${POSITIONAL[@]}" # restore positional parameters

    echo "
        #-----------------------------------------------------------------------
        # $DESCRIPCION
        #-----------------------------------------------------------------------
        XXXXXXXX_process_P$PASO()
        {
                . process_dd.ksh  -name SQL -dsn $CONTORA -disp_status SHR
                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ]
                then
                        sqlplus /nolog @\$SQL ${POSITIONAL[@]}
                        . set_retcode.ksh P$PASO \$?
                fi
        }

        XXXXXXXX_process_ABENP$PASO()
        {
                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ]
                then
                        do_exit
                        . set_retcode.ksh ABENP$PASO \$?
                fi
        }
        "
}

# Copia con un bphxgenr
# Argumentos
# -p, --paso: paso
# -d, --descripcion: descripcion
# -i, --input: fichero a copiar
# -o, --output: fichero copiado
#
# Ejemplos:
# @ copia -p '230' -d 'BPHXGENR-COPIA FICHERO' -i '/home/u028693/proclib/RDWD0301' -o '/home/u028693/RDWD0301'
#
copia()
{
    POSITIONAL=""
    while [[ $# -gt 0 ]]; do
        key="$1"

        case $key in
            -p | --paso)
                PASO="$2"
                shift # past argument
                shift # past value
                ;;
            -d | --descripcion)
                DESCRIPCION="$2"
                shift # past argument
                shift # past value
                ;;
            -i | --input)
                INPUT="$2"
                shift # past argument
                shift # past value
                ;;
            -o | --output)
                OUTPUT="$2"
                shift # past argument
                shift # past value
                ;;
            --args)
                DEFAULT=YES
                shift # past argument
                ;;
            *)                              # unknown option
                POSITIONAL="$POSITIONAL $1" # save it in an array for later
                shift                       # past argument
                ;;
        esac
    done
    set -- "${POSITIONAL[@]}" # restore positional parameters

    echo "
        #-----------------------------------------------------------------------
        # $DESCRIPCION
        #-----------------------------------------------------------------------
        XXXXXXXX_process_P$PASO()
        {

                . process_dd.ksh  -name SYSUT1 -dsn $INPUT   -disp_status SHR
                . process_dd.ksh  -name SYSUT2 -dsn $OUTPUT  -disp_normal PASS -dcbref SYSUT1

                . process_dd.ksh  -name SYSIN -dsn /dev/null

                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ]
                then
                        bphxgenr   <\$SYSIN
                        . set_retcode.ksh P$PASO \$?
                fi
        }

        XXXXXXXX_process_ABENP$PASO()
        {
                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ]
                then
                        do_exit
                        . set_retcode.ksh ABENP$PASO \$?
                fi
        }

        XXXXXXXX_process_FIBEP$PASO()
        {
                . process_dd.ksh  -name DD1 -dsn $OUTPUT  -disp_status OLD -disp_normal CATLG

                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ]
                then
                        echo
                        . set_retcode.ksh FIBEP$PASO \$?
                fi
        }
        "
}

# Unload
# Paso para descargar select de un contora a un fichero
#
# Argumentos:
# -p, --paso: paso
# -d, --descripcion: descripcion
# -u, --unloadcopy: unloadcopy para el fichero a crear
# -i, --input: contora con la select
# -o, --output: fichero a escribir
# -l, --longitud_output: longitud del fichero nuevo
#
# Ejemplos:
# @ unload --paso '025' --descripcion 'UNLOAD-VOLCADO DE DATOS' --unloadcopy /rsicif/sysdes/desxld/unloadcopys/mi_unload_copy --input '/rsicif/sysexi/rsi/cifaprod/contora/mi_contora.sql' --output '/rsicif/syspri/inciba/inc00688/${RSI_PA_DATAP}' -l 34
#
unload()
{
    POSITIONAL=""
    while [[ $# -gt 0 ]]; do
        key="$1"

        case $key in
            -p | --paso)
                PASO="$2"
                shift # past argument
                shift # past value
                ;;
            -d | --descripcion)
                DESCRIPCION="$2"
                shift # past argument
                shift # past value
                ;;
            -u | --unloadcopy)
                UNLOADCOPY="$2"
                shift # past argument
                shift # past value
                ;;
            -i | --input)
                INPUT="$2"
                shift # past argument
                shift # past value
                ;;
            -o | --output)
                OUTPUT="$2"
                shift # past argument
                shift # past value
                ;;
            -l | --longitud_output)
                LONGITUD_OUTPUT="$2"
                shift # past argument
                shift # past value
                ;;
            --args)
                DEFAULT=YES
                shift # past argument
                ;;
            *)                              # unknown option
                POSITIONAL="$POSITIONAL $1" # save it in an array for later
                shift                       # past argument
                ;;
        esac
    done
    set -- "${POSITIONAL[@]}" # restore positional parameters

    echo "
        #-----------------------------------------------------------------------
        # $DESCRIPCION
        #-----------------------------------------------------------------------
        XXXXXXXX_process_P$PASO()
        {
                . process_dd.ksh  -name SYSIN    -dsn $INPUT -disp_status SHR
                . process_dd.ksh  -name SYSREC00 -dsn $OUTPUT -disp_normal PASS  -lrecl $LONGITUD_OUTPUT -recfm FB

                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ] ; then
                        export UNLOADCOPY00=$UNLOADCOPY
                        /opt/hpbatch/unload/UNLOAD
                        . set_retcode.ksh P$PASO \$?
                        . process_dd.ksh SET_RECDAT \$SYSREC00 FB.$LONGITUD_OUTPUT
                        unset UNLOADCOPY00
                fi
        }
        XXXXXXXX_process_ABENP$PASO()
        {
                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ] ; then
                        do_exit
                        . set_retcode.ksh ABENP$PASO \$?
                fi
        }
        XXXXXXXX_process_FIBEP$PASO()
        {
                . process_dd.ksh -name DD1 -dsn $OUTPUT -disp_status OLD -disp_normal CATLG

                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ] ; then
                        echo "FIN DE DESCARGA OK"
                        . set_retcode.ksh FIBEP$PASO \$?
                fi
        }
        "

}

# EXPCAMBI
# Sustituye valores
#
# Argumentos:
# -p, --paso: paso
# -d, --descripcion: descripcion
# -i, --input: archivo a modificar
# -o, --output: archivo con las sustituciones
# -a, --argumentos: string con pares palabras-clave/sustituciones
#
# Ejemplos:
# @ expcambi -p '010' -d 'EXPCAMBI-SUSTITUYE FECHA EN SQL' --input '/rsicif/sysdes/desxld/contora/mi_contora.sql' --output '/rsicif/syspwi/rdwd6881.p10' --argumentos '%%FECHA_PROCESO ${RSI_PA_FEC3P} %%ENT_SAREB ${RSI_PA_ENTI} %%CAP_ID_SET ${RSI_PA_IDSET}'
#
expcambi()
{

    POSITIONAL=""
    while [[ $# -gt 0 ]]; do
        key="$1"

        case $key in
            -p | --paso)
                PASO="$2"
                shift # past argument
                shift # past value
                ;;
            -d | --descripcion)
                DESCRIPCION="$2"
                shift # past argument
                shift # past value
                ;;
            -i | --input)
                INPUT="$2"
                shift # past argument
                shift # past value
                ;;
            -o | --output)
                OUTPUT="$2"
                shift # past argument
                shift # past value
                ;;
            -a | --argumentos)
                ARGUMENTOS="$2"
                shift # past argument
                shift # past value
                ;;
            --args)
                DEFAULT=YES
                shift # past argument
                ;;
            *)                              # unknown option
                POSITIONAL="$POSITIONAL $1" # save it in an array for later
                shift                       # past argument
                ;;
        esac
    done
    set -- "${POSITIONAL[@]}" # restore positional parameters

    echo "
        #-----------------------------------------------------------------------
        # $DESCRIPCION
        #-----------------------------------------------------------------------
        XXXXXXXX_process_P$PASO()
        {
                . process_dd.ksh  -name ENTRADA -dsn $INPUT  -disp_status SHR
                . process_dd.ksh  -name SALIDA  -dsn $OUTPUT -disp_normal PASS -lrecl 80 -recfm FB

                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ]
                then
                        export PATH=\${RSI_BASICPATH}:/rsicif/sysexi/rsi/prod/exec
                        expcambi $ARGUMENTOS
                        . set_retcode.ksh P$PASO \$?
                        export PATH=\${RSI_BASICPATH}
                fi
        }
        XXXXXXXX_process_ABENP$PASO()
        {
                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ]
                then
                        do_exit
                        . set_retcode.ksh ABENP$PASO \$?
                fi
        }
        XXXXXXXX_process_FIBEP$PASO()
        {
                . process_dd.ksh  -name DD1 -dsn $OUTPUT  -disp_status OLD -disp_normal CATLG

                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ]
                then
                        . set_retcode.ksh FIBEP$PASO \$?
                fi
        }
        "
}

# Cruce Sort
# Cruce de dos ficheros con un ssort
#
# Argumentos:
# -p, --paso: paso
# -d, --descripcion: descripcion
# -i, --input1: archivo 1 a cruzar
# -i2, --input2: archivo 2 a cruzar
# -o, --output: archivo de salida de la control
# -l, --longitud_output: longitud del archivo de salida
# -c, --control: control para hacer el cruce de ficheros
#
# Ejemplos:
# @ cruce_sort --paso '010' --descripcion 'SSORT-CRUCE DE FICHEROS' --input1 '/rsicif/syspwi/rdwcba.sysrec00.p020.${RSI_PA_DATAP}' --input2 '/lnxbcip1/rsicif/syspri/rdwcba/rdw00684/${RSI_PA_DATAMNY2}' --output '/rsicif/syspri/rdwcba/rdw00852/${RSI_PA_DATAMNY1}' --longitud_output 32 --control '/rsicif/sysexi/rsi/cifaprod/control/rdwms471'
cruce_sort()
{

    POSITIONAL=""
    while [[ $# -gt 0 ]]; do
        key="$1"

        case $key in
            -p | --paso)
                PASO="$2"
                shift # past argument
                shift # past value
                ;;
            -d | --descripcion)
                DESCRIPCION="$2"
                shift # past argument
                shift # past value
                ;;
            -i1 | --input1)
                INPUT1="$2"
                shift # past argument
                shift # past value
                ;;
            -i2 | --input2)
                INPUT2="$2"
                shift # past argument
                shift # past value
                ;;
            -o | --output)
                OUTPUT="$2"
                shift # past argument
                shift # past value
                ;;
            -l | --longitud_output)
                LONGITUD_OUTPUT="$2"
                shift # past argument
                shift # past value
                ;;
            -c | --control)
                CONTROL="$2"
                shift # past argument
                shift # past value
                ;;
            --args)
                DEFAULT=YES
                shift # past argument
                ;;
            *)                              # unknown option
                POSITIONAL="$POSITIONAL $1" # save it in an array for later
                shift                       # past argument
                ;;
        esac
    done
    set -- "${POSITIONAL[@]}" # restore positional parameters

    echo "
        #-----------------------------------------------------------------------
        # $DESCRIPCION
        #-----------------------------------------------------------------------
        XXXXXXXX_process_P$PASO()
        {
                . process_dd.ksh  -name INFILE1  -dsn $INPUT1  -disp_status SHR
                . process_dd.ksh  -name INFILE2  -dsn $INPUT2  -disp_status SHR
                . process_dd.ksh  -name OUTFILE1 -dsn $OUTPUT  -disp_normal PASS -lrecl $LONGITUD_OUTPUT -recfm FB
                . process_dd.ksh  -name SYSIN    -dsn $CONTROL -disp_status SHR

                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ]
                then
                        ssort  /statistics /collatingsequence default ASCII < \$SYSIN 2>&1
                        . set_retcode.ksh P$PASO \$?
                fi
        }
        XXXXXXXX_process_ABENP$PASO()
        {

                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ]
                then
                        do_exit
                        . set_retcode.ksh ABENP$PASO \$?
                fi
        }
        XXXXXXXX_process_FIBEP$PASO()
        {
                . process_dd.ksh  -name DD1 -dsn $OUTPUT     -disp_status OLD -disp_normal CATLG

                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ]
                then
                        echo
                        . set_retcode.ksh FIBEP$PASO \$?
                fi
        }
        "
}

# Sort
# Ordena un fichero
#
# Argumentos:
# -p, --paso: paso
# -d, --descripcion: descripcion
# -i, --sortin fichero de entrada
# -li, --longitud_input: longitud del archivo de entrada
# -o, --sortout: fichero de salida
# -lo, --longitud_output: longitud del archivo de salida
# -c, --ocntrol: archivo control
#
# Ejemplos:
# @ syncsort --paso '010' --descripcion 'SYNCSORT-ORDENA EL FICHERO' --sortin '/rsicif/syspri/bsccba/bsc00140/${RSI_PA_DATAP}.fus' --longitud_input 32 --sortout '/rsicif/syspwi/bsccba.bscv1321.descarga.p030.${RSI_PA_DATAP}' --longitud_output 10 --control '/rsicif/sysexi/rsi/cifaprod/control/rdwms471'
syncsort()
{

    POSITIONAL=""
    while [[ $# -gt 0 ]]; do
        key="$1"

        case $key in
            -p | --paso)
                PASO="$2"
                shift # past argument
                shift # past value
                ;;
            -d | --descripcion)
                DESCRIPCION="$2"
                shift # past argument
                shift # past value
                ;;
            -i | --sortin)
                SORTIN="$2"
                shift # past argument
                shift # past value
                ;;
            -o | --sortout)
                SORTOUT="$2"
                shift # past argument
                shift # past value
                ;;
            -lo | --longitud_output)
                LONGITUD_OUTPUT="$2"
                shift # past argument
                shift # past value
                ;;
            -li | --longitud_input)
                LONGITUD_INPUT="$2"
                shift # past argument
                shift # past value
                ;;
            -c | --control)
                CONTROL="$2"
                shift # past argument
                shift # past value
                ;;
            --args)
                DEFAULT=YES
                shift # past argument
                ;;
            *)                              # unknown option
                POSITIONAL="$POSITIONAL $1" # save it in an array for later
                shift                       # past argument
                ;;
        esac
    done
    set -- "${POSITIONAL[@]}" # restore positional parameters

    echo "
        #-----------------------------------------------------------------------
        # $DESCRIPCION
        #-----------------------------------------------------------------------
        XXXXXXXX_process_P${PASO}()
        {
                . process_dd.ksh  -name SORTIN  -dsn $SORTIN -disp_status SHR -lrecl $LONGITUD_INPUT -recfm FB
                . process_dd.ksh  -name SORTOUT -dsn $SORTOUT -disp_normal PASS -lrecl $LONGITUD_OUTPUT -recfm FB
                . process_dd.ksh  -name SYSIN   -dsn $CONTROL -disp_status SHR -lrecl 80 -recfm FB

                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ]
                then
                        call_syncsort.ksh -tmpfile \$RSI_TEMP_DIR/syncsort_\$RSI_STEPNAME.\$\$   <\$SYSIN
                        . set_retcode.ksh P${PASO} \$?
                fi
        }

        XXXXXXXX_process_ABENP${PASO}()
        {
                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ]
                then
                        do_exit
                        . set_retcode.ksh ABENP${PASO} \$?
                fi
        }

        XXXXXXXX_process_FIBEP${PASO}()
        {
                . process_dd.ksh  -name DD1 -dsn $SORTOUT -disp_status OLD -disp_normal CATLG
                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ]
                then
                        echo
                        . set_retcode.ksh FIBEP${PASO} \$?
                fi
        }
        "
}

# VERIFICA FICHERO
# Verifica la existencia de un fichero
#
# Argumentos:
# -p, --paso: paso
# -d, --descripcion: descripcion
# -i, --input: archivo a comprobar que existe
#
# Ejemplos:
# @ verifica_fichero --paso '070' --descripcion 'VERIFICACION SI EL FICHERO TIENE DATOS' --input '/rsicif/syspri/ifccba/ifc00018/${RSI_PA_DATAP}'
verifica_fichero()
{

    POSITIONAL=""
    while [[ $# -gt 0 ]]; do
        key="$1"

        case $key in
            -p | --paso)
                PASO="$2"
                shift # past argument
                shift # past value
                ;;
            -d | --descripcion)
                DESCRIPCION="$2"
                shift # past argument
                shift # past value
                ;;
            -i | --input)
                INPUT="$2"
                shift # past argument
                shift # past value
                ;;
            --args)
                DEFAULT=YES
                shift # past argument
                ;;
            *)                              # unknown option
                POSITIONAL="$POSITIONAL $1" # save it in an array for later
                shift                       # past argument
                ;;
        esac
    done
    set -- "${POSITIONAL[@]}" # restore positional parameters

    echo "
        #-----------------------------------------------------------------------
        # $DESCRIPCION
        #-----------------------------------------------------------------------
        XXXXXXXX_process_P$PASO()
        {
                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ]
                then
                        export PATH=\${RSI_BASICPATH}
                        nofile.ksh $INPUT
                        . set_retcode.ksh P$PASO \$?

                fi
        }
        XXXXXXXX_process_ABENP$PASO()
        {
                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ]
                then
                        do_exit
                        . set_retcode.ksh ABENP$PASO \$?
                fi
        }
        "
}

# putparm
# Escribe una linea en un fichero nuevo
#
# Argumentos:
# -p, --paso: paso
# -d, --descripcion: descripcion
# -f, --file: archivo donde escribit
# -i, --input: texto a escribir en el nuevo archivo
#
# Ejemplos:
# @ putparm -p '010' -d 'PUTPARM PARA HACER MOVE' --file '/rsicif/syspwi/texcba.tex00029s.p160' --input '/ ALTER /rsicif/syspri/texcba/tex00n22s - '
putparm()
{

    POSITIONAL=""
    while [[ $# -gt 0 ]]; do
        key="$1"

        case $key in
            -p | --paso)
                PASO="$2"
                shift # past argument
                shift # past value
                ;;
            -d | --descripcion)
                DESCRIPCION="$2"
                shift # past argument
                shift # past value
                ;;
            -f | --file)
                FILE="$2"
                shift # past argument
                shift # past value
                ;;
            -i | --input)
                INPUT="$2"
                shift # past argument
                shift # past value
                ;;
            --args)
                DEFAULT=YES
                shift # past argument
                ;;
            *)                              # unknown option
                POSITIONAL="$POSITIONAL $1" # save it in an array for later
                shift                       # past argument
                ;;
        esac
    done
    set -- "${POSITIONAL[@]}" # restore positional parameters

    echo "
        #-----------------------------------------------------------------------
        # $DESCRIPCION
        #-----------------------------------------------------------------------
        XXXXXXXX_process_P$PASO()
        {
                . process_dd.ksh  -name OUTPARM -dsn $FILE -disp_normal PASS -lrecl 80 -recfm FB

                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ] ; then
                        putparm "$INPUT"   
                        . set_retcode.ksh P$PASO \$?
                fi
        }
        XXXXXXXX_process_ABENP$PASO()
        {

                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ] ; then
                        do_exit    
                        . set_retcode.ksh ABENP$PASO \$?
                fi
        }
        XXXXXXXX_process_FIBEP$PASO()
        {
                . process_dd.ksh  -name DD1 -dsn $FILE -disp_status OLD -disp_normal CATLG

                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ] ; then
                        echo    
                        . set_retcode.ksh FIBEP$PASO \$?
                fi
        }
        "
}

# COBOL
# Ejecuta un programa COBOL
#
# Argumentos:
# -p, --paso: paso
# -d, --descripcion: descripcion
# -c, --cobol: programa cobol a ejecutar
# Una o mas sequencias de las siguientes opciones para declarar ficheros de entrada o salida al cobol
# el simbolo * significa que puedes añadir cualquier cosa en su lugar, numeros, letras u otros simbolos.
# -n*, --nombre*: nombre de la vista que se va a usar en el COBOL
# -f*, --fichero*: ruta del fichero
# -t*, --tipo*: escribe 'salida' si el fichero va a ser de salida, en caso contrario se tomara como de entrada
# -l*, --longitud*: longitud del fichero
#
# Ejemplos:
# @ cobol_paso -p '100' -d 'EJECUCIÓN PROGRAMA COBOL' -c 'CPVD0023' -n1 'ENTRADA1' --fichero1 '/rsicif/syspwi/entrada1' --tipo1 'entrada' --longitud1 '34' -n2 'SALIDA1' --fichero2 '/rsicif/syspwi/salida_p010' --tipo2 'salida' --longitud2 '54' -n3 'OUTPUT' --fichero3 '/rsicif/syspwi/wasaaaap' --tipo3 'salida' --longitud3 '200'
cobol_paso()
{

    NOMBRES=""
    FICHEROS=""
    TIPOS=""
    LONGITUDES=""

    POSITIONAL=""
    while [[ $# -gt 0 ]]; do
        key="$1"

        case $key in
            -p | --paso)
                PASO="$2"
                shift # past argument
                shift # past value
                ;;
            -d | --descripcion)
                DESCRIPCION="$2"
                shift # past argument
                shift # past value
                ;;
            -c | --cobol)
                COBOL="$2"
                shift # past argument
                shift # past value
                ;;
            -n* | --nombre*)
                NOMBRES="$NOMBRES"" $2"
                shift # past argument
                shift # past value
                ;;
            -f* | --fichero*)
                FICHEROS=$FICHEROS" $2"
                shift # past argument
                shift # past value
                ;;
            -t* | --tipo*)
                TIPOS=$TIPOS" $2"
                shift # past argument
                shift # past value
                ;;
            -l* | --longitud*)
                LONGITUDES=$LONGITUDES" $2"
                shift # past argument
                shift # past value
                ;;
            --args)
                DEFAULT=YES
                shift # past argument
                ;;
            *)                              # unknown option
                POSITIONAL="$POSITIONAL $1" # save it in an array for later
                shift                       # past argument
                ;;
        esac
    done
    set -- "${POSITIONAL[@]}" # restore positional parameters

    process_dds=""
    fibe_process_dds=""
    N_FICS=$(echo "$FICHEROS" | sed 's/^ *//' | tr -s ' ' '\n' | wc -l)

    for i in $(seq 1 $N_FICS); do

        NOMBRE=$(echo "$NOMBRES" | sed 's/^ *//' | tr -s ' ' '\n' | sed -n ${i}p)
        FICHERO=$(echo "$FICHEROS" | sed 's/^ *//' | tr -s ' ' '\n' | sed -n ${i}p)
        TIPO=$(echo "$TIPOS" | sed 's/^ *//' | tr -s ' ' '\n' | sed -n ${i}p)
        TIPO=$(echo "$TIPO" | tr '[:upper:]' '[:lower:]')
        LONGITUD=$(echo "$LONGITUDES" | sed 's/^ *//' | tr -s ' ' '\n' | sed -n ${i}p)

        if [ "${TIPO}" = "salida" ]; then
            process_dds="$process_dds""
                        . process_dd.ksh -name $NOMBRE  -dsn $FICHERO -disp_normal PASS -lrecl $LONGITUD -recfm FB"

            fibe_process_dds="$fibe_process_dds""
                        . process_dd.ksh -name DD${i} -dsn $FICHERO -disp_status OLD -disp_normal CATLG"
        else
            process_dds="$process_dds""
                        . process_dd.ksh -name $NOMBRE -dsn $FICHERO -disp_status SHR -lrecl $LONGITUD -recfm FB"
        fi
    done

    echo "
        #-----------------------------------------------------------------------
        # $DESCRIPCION
        #-----------------------------------------------------------------------
        XXXXXXXX_process_P${PASO}()
        {
                ${process_dds}

                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ]; then
                        export PATH=\${RSI_BASICPATH}
                        ${COBOL}
                        . set_retcode.ksh P${PASO} \$?
                        export PATH=\${RSI_BASICPATH}
                fi
        }
        XXXXXXXX_process_ABENP${PASO}()
        {
                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ]; then
                        do_exit
                        . set_retcode.ksh ABENP${PASO} \$?
                fi
        }
        XXXXXXXX_process_FIBEP${PASO}()
        {
                ${fibe_process_dds}

                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ]; then
                        echo
                        . set_retcode.ksh FIBEP${PASO} \$?
                fi
        }
        "

}

# CONCATENA
# Concatena dos o mas ficheros
#
# Argumentos:
# -p, --paso: paso
# -d, --descripcion: descripcion
# -c, --control: control con el siguiente contenido: `      SORT FIELDS=COPY`
# -o, --output: fichero de salida concatenado
# -l, --longitud: longitud de los ficheros
# Uno o mas ficheros
# el simbolo * significa que puedes añadir cualquier cosa en su lugar, numeros, letras u otros simbolos.
# -f*, --fichero*: ruta del fichero
#
# Ejemplos:
# @ concat -p '120' -d 'SYNCSORT-SORT PARA CONCATENAR FICHEROS' -c '/rsicif/sysdes/desxld/control/mi_control' -o '/rsicif/syspri/ifccba/ifc00174' --longitud '21' --fichero1 '/rsicif/syspwi/entrada1' --fichero2 '/rsicif/syspwi/salida_p010' --fichero3 '/rsicif/syspwi/otro_archivo'

concat()
{

    FICHEROS=""

    POSITIONAL=""
    while [[ $# -gt 0 ]]; do
        key="$1"

        case $key in
            -p | --paso)
                PASO="$2"
                shift # past argument
                shift # past value
                ;;
            -d | --descripcion)
                DESCRIPCION="$2"
                shift # past argument
                shift # past value
                ;;
            -c | --control)
                CONTROL="$2"
                shift # past argument
                shift # past value
                ;;
            -c | --output)
                OUTPUT="$2"
                shift # past argument
                shift # past value
                ;;
            -o | --output)
                OUTPUT="$2"
                shift # past argument
                shift # past value
                ;;
            -f* | --fichero*)
                FICHEROS=$FICHEROS" $2"
                shift # past argument
                shift # past value
                ;;
            -l | --longitud)
                LONGITUD="$2"
                shift # past argument
                shift # past value
                ;;
            --args)
                DEFAULT=YES
                shift # past argument
                ;;
            *)                              # unknown option
                POSITIONAL="$POSITIONAL $1" # save it in an array for later
                shift                       # past argument
                ;;
        esac
    done
    set -- "${POSITIONAL[@]}" # restore positional parameters

    concat_line=""
    N_FICS=$(echo "$FICHEROS" | sed 's/^ *//' | tr -s ' ' '\n' | wc -l)

    for i in $(seq 1 $N_FICS); do

        FICHERO=$(echo "$FICHEROS" | sed 's/^ *//' | tr -s ' ' '\n' | sed -n ${i}p)

        if [ "$i" = "1" ]; then
            concat_line=". process_dd.ksh -name SORTIN -dsn $FICHERO -disp_status SHR -lrecl $LONGITUD -recfm FB \\"
        elif [ "$i" = "$N_FICS" ]; then
            concat_line="$concat_line""
                        -concat  -dsn $FICHERO -disp_status SHR -lrecl $LONGITUD -recfm FB"
        else
            concat_line="$concat_line""
                        -concat  -dsn $FICHERO -disp_status SHR -lrecl $LONGITUD -recfm FB \\"
        fi
    done

    echo "
        #---------------------------------------------------------------
        # $DESCRIPCION
        #---------------------------------------------------------------
        XXXXXXXX_process_P${PASO}()
        {
                . process_dd.ksh -name SYSIN -dsn $CONTROL -disp_status SHR

                $concat_line

                . process_dd.ksh  -name SORTOUT  -dsn $OUTPUT -disp_normal PASS -lrecl $LONGITUD -recfm FB

                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ] ; then
                        call_syncsort.ksh -tmpfile \$RSI_TEMP_DIR/syncsort_\$RSI_STEPNAME.\$\$   <\$SYSIN
                        . set_retcode.ksh P${PASO} \$?
                fi
        }
        XXXXXXXX_process_ABENP${PASO}()
        {
                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ] ; then
                        do_exit
                        . set_retcode.ksh ABENP${PASO} \$?
                fi
        }
        XXXXXXXX_process_FIBEP${PASO}()
        {
                . process_dd.ksh  -name DD1 -dsn $OUTPUT -disp_status OLD -disp_normal CATLG

                if  [ \"\$RSI_PHASE\" = \"RUN\" -a \"\$RSI_SUBPHASE\" = \"PRERUN\" ] ; then
                        echo
                        . set_retcode.ksh FIBEP${PASO} \$?
                fi
        }
        "

}
