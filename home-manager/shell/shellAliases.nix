{ pkgs }:
let
  user = "u028693";
in {
  # Basic
  ll = "ls -la";
  vim = "nvim";
  vi = "nvim";

  # Directory aliases
  vimconfig = "cd ~/.config/nvim";
  notas = "cd ~/Documents/notas";
  repls = "cd ~/dev/repls";

  # Command aliases
  open = "xdg-open";
  avr-gcc = "${pkgs.arduino}/share/arduino/hardware/tools/avr/bin/avr-gcc";
  vimt = "vim --cmd term"; # Vim terminal emulator
  ssh = "TERM=xterm-color; ssh";

  # Machines
  #aix = "sshpass -p ${pass} ssh ${user}@172.22.2.83";
  aix = "ssh ${user}@172.22.2.83";
  ci_desa = ''
    mkdir ~/machines/$CI_DESA 2> /dev/null;
    cd ~/machines/$CI_DESA;
    sshfs u028693@172.22.5.193:/ ~/machines/$CI_DESA/;
    load_env_dirs $CI_DESA;
  '';
  ci_prod = ''
    # lnxbcip1
    mkdir ~/machines/$CI_PROD 2> /dev/null;
    cd ~/machines/$CI_PROD;
    sshfs u028693@172.22.7.26:/ ~/machines/$CI_PROD/;
    load_env_dirs $CI_PROD;
  '';
  iris_desa = ''
    mkdir ~/machines/$IRIS_DESA 2> /dev/null;
    cd ~/machines/$IRIS_DESA;
    sshfs u028693@10.1.232.12:/ ~/machines/$IRIS_DESA/;
    load_env_dirs $IRIS_DESA;
  '';
  iris_prod = ''
    mkdir ~/machines/$IRIS_PROD 2> /dev/null;
    cd ~/machines/$IRIS_PROD;
    sshfs u028693@10.1.232.13:/ ~/machines/$IRIS_PROD/;
    load_env_dirs $IRIS_PROD;
  '';
}
