```sh
git clone git@gitlab.com:luisdomingoaranda/nix.git
git-crypt unlock
sudo nixos-rebuild switch --upgrade --flake .#rpi4 --target-host root@luisdomingoaranda.com
# or
home-manager switch --flake .#arc
```
