{
  description = "NixOS and home-manager config";

  inputs = {
    nixpkgs-unstable = {
      url = "github:nixos/nixpkgs/nixos-unstable";
    };
    nixpkgs-stable = {
      url = "github:nixos/nixpkgs/nixos-24.11";
    };
    home-manager-stable = {
      url = "github:nix-community/home-manager/release-24.05";
      inputs.nixpkgs.follows = "nixpkgs-stable";
    };
    home-manager-unstable = {
      url = "github:nix-community/home-manager"; # master branch
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };
    mynixpkgs = {
      url = "gitlab:luisdomingoaranda/mynixpkgs";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };
    nixos-hardware = {
      url = "github:nixos/nixos-hardware";
    };
    nix-colors = {
      url = "github:misterio77/nix-colors";
    };
    nixgl = {
      url = "github:nix-community/nixGL";
    };
    openconnect-sso = {
      url = "github:ThinkChaos/openconnect-sso/fix/nix-flake";
    };
    nix-snapd = {
      url = "github:io12/nix-snapd";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };
  };

  outputs =
    { self
    , ...
    } @ inputs:
    {
      homeConfigurations = import ./home-manager inputs;
      nixosConfigurations = import ./nixos inputs;
      images = {
        # sd-image-aarch64 module is only needed to build the image
        # After boot, the rpi4 nixosConfiguration doesn't need it
        rpi4 = (self.nixosConfigurations.rpi4.extendModules {
          modules = [ "${inputs.nixpkgs-unstable}/nixos/modules/installer/sd-card/sd-image-aarch64.nix" ];
        }).config.system.build.sdImage;
      };
    };
}
